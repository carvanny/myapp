package com.br.kondon.model;

public enum TipoMensagem {

	SUCESSO("btn-success"),
	ERRO("btn-danger"),
	PADRAO("btn-primary");
	
	private final String nome;

	private TipoMensagem(String nome) {
		this.nome = nome;
	}

	public String getNome() {
		return nome;
	}
	
    @Override
    public String toString() {
        return getNome();
    }

}
