package com.br.kondon.model;

import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "correspondencia")
public class Correspondencia extends AbstractEntity {

	private static final long serialVersionUID = 1L;

	@Column(name = "bloco", nullable = false)
	private String bloco;
	
	@Column(name = "apartamento", nullable = false)
	private String apartamento;
	
	
	@Column(name = "dataCadastro", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern=PATTERN_DATE)
	private Calendar dataCadastro;

	@Column(name = "dataAlteracao")
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern=PATTERN_DATE_TIME)
	private Calendar dataAlteracao;

	public String getBloco() {
		return bloco;
	}

	public void setBloco(String bloco) {
		this.bloco = bloco;
	}

	public String getApartamento() {
		return apartamento;
	}

	public void setApartamento(String apartamento) {
		this.apartamento = apartamento;
	}

	public Calendar getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Calendar dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public Calendar getDataAlteracao() {
		return dataAlteracao;
	}

	public void setDataAlteracao(Calendar dataAlteracao) {
		this.dataAlteracao = dataAlteracao;
	}
	
	
		
}
