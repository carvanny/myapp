package com.br.kondon.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.br.kondon.model.Correspondencia;
import com.br.kondon.service.CorrespondenciaService;


@Controller
public class CorrespondenciaController {
	
	@Autowired
	private CorrespondenciaService correspondenciaService;

	
	
	@RequestMapping(value="/admin/correspondencia", method = RequestMethod.GET)
	public ModelAndView registration(){
		ModelAndView modelAndView = new ModelAndView();
		Correspondencia correspondencia = new Correspondencia();
		modelAndView.addObject("correspondencia", correspondencia);
		modelAndView.setViewName("admin/correspondencia");
		return modelAndView;
	}
	
	@RequestMapping(value = "/admin/correspondencia", method = RequestMethod.POST)
	public ModelAndView createNewUser(@Valid Correspondencia correspondencia, BindingResult bindingResult) {
		ModelAndView modelAndView = new ModelAndView();
		correspondenciaService.saveCorrespondencia(correspondencia);
		//Correspondencia c = correspondenciaService.findCorrespondenciaByApartamento("1202");
		
		/*
		//User userExists = userService.findUserByEmail(user.getEmail());
		if (userExists != null) {
			bindingResult
					.rejectValue("email", "error.user",
							"There is already a user registered with the email provided");
		}
		if (bindingResult.hasErrors()) {
			modelAndView.setViewName("registration");
		} else {
			userService.saveUser(user);
			modelAndView.addObject("successMessage", "User has been registered successfully");
			modelAndView.addObject("user", new User());
			modelAndView.setViewName("registration");
			
		}*/
		return modelAndView;
	}
	
//	@RequestMapping(value="/admin/home", method = RequestMethod.GET)
//	public ModelAndView home(){
//		ModelAndView modelAndView = new ModelAndView();
//		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
//		User user = userService.findUserByEmail(auth.getName());
//		modelAndView.addObject("userName",  user.getName() + " " + user.getLastName() + " (" + user.getEmail() + ")");
//		modelAndView.addObject("adminMessage","Content Available Only for Users with Admin Role");
//		modelAndView.setViewName("admin/home");
//		return modelAndView;
//	}
	

}
