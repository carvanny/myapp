package com.br.kondon.service;

import com.br.kondon.model.User;

public interface UserService {
	public User findUserByEmail(String email);
	public void saveUser(User user);
}
