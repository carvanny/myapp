package com.br.kondon.service;

import com.br.kondon.model.Correspondencia;

public interface CorrespondenciaService {
	public Correspondencia findCorrespondenciaByApartamento(String ap);
	
	public void saveCorrespondencia(Correspondencia obj);
}
