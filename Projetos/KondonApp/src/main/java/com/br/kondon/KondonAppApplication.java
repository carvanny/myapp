package com.br.kondon;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KondonAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(KondonAppApplication.class, args);
	}
}
