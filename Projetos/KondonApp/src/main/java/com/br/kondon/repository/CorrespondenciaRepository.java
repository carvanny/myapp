package com.br.kondon.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.br.kondon.model.Correspondencia;


@Repository("correspondenciaRepository")
public interface CorrespondenciaRepository extends JpaRepository<Correspondencia, Long> {
	Correspondencia findByApartamento(String ap);
}
