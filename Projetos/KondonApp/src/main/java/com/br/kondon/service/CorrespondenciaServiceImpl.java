package com.br.kondon.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.br.kondon.model.Correspondencia;
import com.br.kondon.repository.CorrespondenciaRepository;

@Service("correspondenciaService")
public class CorrespondenciaServiceImpl implements CorrespondenciaService {

	@Autowired
	private CorrespondenciaRepository correspondenciaRepository;

	@Override
	public Correspondencia findCorrespondenciaByApartamento(String ap) {
		return correspondenciaRepository.findByApartamento(ap);
	}

	@Override
	public void saveCorrespondencia(Correspondencia obj) {
		correspondenciaRepository.save(obj);
	}

}
