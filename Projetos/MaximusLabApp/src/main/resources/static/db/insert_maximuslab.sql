#Cargo do sistema
INSERT INTO `dbmaximuslab`.`cargo` (`id`, `nome`) VALUES ('1', 'Diretor');
INSERT INTO `dbmaximuslab`.`cargo` (`id`, `nome`) VALUES ('2', 'Gerente');
INSERT INTO `dbmaximuslab`.`cargo` (`id`, `nome`) VALUES ('3', 'Tecnico');

#Role do sistema
INSERT INTO `dbmaximuslab`.`role` (`role_id`, `role`) VALUES ('1', 'ADMIN');
INSERT INTO `dbmaximuslab`.`role` (`role_id`, `role`) VALUES ('2', 'DIRETORIA');
INSERT INTO `dbmaximuslab`.`role` (`role_id`, `role`) VALUES ('3', 'GERENCIA');
INSERT INTO `dbmaximuslab`.`role` (`role_id`, `role`) VALUES ('4', 'TECNICO');
