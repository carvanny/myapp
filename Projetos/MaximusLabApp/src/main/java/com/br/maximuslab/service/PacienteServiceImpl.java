package com.br.maximuslab.service;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.br.maximuslab.model.Paciente;
import com.br.maximuslab.repository.PacienteRepository;

@Service("pacienteService")
public class PacienteServiceImpl implements PacienteService {

	@Autowired
	private PacienteRepository pacienteRepository;
	
	@Override
	public List<Paciente> findByNomeOrCpfOrRg(String nome, String cpf, String rg){		
		return pacienteRepository.findByNomeOrCpfOrRg(nome, cpf, rg);
	}
	
	@Override
	public List<Paciente> findByAll(){		
		return pacienteRepository.findByAll();
	}
	
//	@Override
//	public List<Paciente> findByDataCadastrado(LocalDate dataInicio, LocalDate dataFim){
//		return pacienteRepository.findByDataCadastrado(dataInicio, dataFim);
//	}

//	@Override
//	public void saveOrUpdate(Paciente paciente) {
//		pacienteRepository.save(paciente);		
//	}
//
//	@Override
//	public void deletePaciente(Paciente paciente) {
//		pacienteRepository.delete(paciente);
//		
//	}
}
