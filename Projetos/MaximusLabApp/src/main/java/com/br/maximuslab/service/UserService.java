package com.br.maximuslab.service;

import com.br.maximuslab.model.User;

public interface UserService {
	public User findUserByEmail(String email);
	public void saveUser(User user);
}
