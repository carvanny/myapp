package com.br.maximuslab.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.br.maximuslab.model.Funcionario;
import com.br.maximuslab.repository.FuncionarioRepository;

@Service("funcionarioService")
public class FuncionarioServiceImpl implements FuncionarioService {

	@Autowired
	private FuncionarioRepository funcionarioRepository;
	
	@Override
	public List<Funcionario> findByNomeOrCpfOrRg(String nome,String cpf, String rg) {
		return funcionarioRepository.findByNomeOrCpfOrRg(nome, cpf, rg);
	}

	@Override
	public void saveOrUpdateFuncionario(Funcionario funcionario) {
		funcionarioRepository.save(funcionario);		
	}

}
