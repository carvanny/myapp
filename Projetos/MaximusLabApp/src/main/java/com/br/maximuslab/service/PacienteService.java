package com.br.maximuslab.service;

import java.util.List;

import com.br.maximuslab.model.Paciente;

public interface PacienteService {
	public List<Paciente> findByNomeOrCpfOrRg(String nome, String cpf, String rg);
	public List<Paciente> findByAll();
	//public List<Paciente> findByDataCadastrado(LocalDate dataInicio, LocalDate dataFim);
//	public void saveOrUpdate(Paciente paciente);
//	public void deletePaciente(Paciente paciente);
}
