package com.br.maximuslab.model;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.annotations.GenericGenerator;

/**
*
* @author carvanny
*/
@Entity
@Table(name = "ATENDIMENTO", catalog = "", schema = "APP")
@XmlRootElement
public class Atendimento implements Serializable {

	private static final long serialVersionUID = 1L;
	
    @Id
    @GenericGenerator(name="kaugen" , strategy="increment")
    @GeneratedValue(generator="kaugen")
    @Column(name="ID")
	private long id;
    
    @Column(name = "HORA")
	private LocalDateTime hora;
    
    @Column(name = "DATA")
	private LocalDate data;
	
    private Exame exame;
	private Funcionario funcionario;
	private Paciente paciente;
	private Convenio convenio;
	
}
