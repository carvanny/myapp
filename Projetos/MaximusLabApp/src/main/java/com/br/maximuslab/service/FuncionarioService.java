package com.br.maximuslab.service;

import java.util.List;

import com.br.maximuslab.model.Funcionario;

public interface FuncionarioService {
	public List<Funcionario> findByNomeOrCpfOrRg(String nome,String cpf, String rg);
	public void saveOrUpdateFuncionario(Funcionario funcionario);	
}
