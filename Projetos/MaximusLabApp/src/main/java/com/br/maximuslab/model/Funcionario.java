package com.br.maximuslab.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.constraints.NotEmpty;

/**
*
* @author carvanny
*/
@Entity
@Table(name = "FUNCIONARIO", catalog = "", schema = "APP")
@XmlRootElement
public class Funcionario implements Serializable {

	private static final long serialVersionUID = 1L;

    @Id
    @GenericGenerator(name="kaugen" , strategy="increment")
    @GeneratedValue(generator="kaugen")
    @Column(name="ID")
	private long id;
    
    @NotEmpty(message = "*Informe o nome do funcionário")
    @Column(name = "NOME")
	private String nome;
    
    @Column(name = "CPF")
	private String cpf;
    
    @Column(name = "RG")
	private String rg;
    
    @Column(name = "REGISTRO_MEDICO")
    private String registroMedico;
    
    @Column(name = "CARGO")
	private Cargo cargo;
    
}
