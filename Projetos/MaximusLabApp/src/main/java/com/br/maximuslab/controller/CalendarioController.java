package com.br.maximuslab.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;


@Controller
public class CalendarioController {
	
	
	@RequestMapping(value="/calendario", method = RequestMethod.GET)
	public ModelAndView calendario(){
		ModelAndView modelAndView = new ModelAndView();
		//Paciente paciente = new Paciente();
		//modelAndView.addObject("paciente", paciente);
		modelAndView.setViewName("admin/calendar");
		return modelAndView;
	}

}
