package com.br.maximuslab.controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.br.maximuslab.model.Paciente;
import com.br.maximuslab.service.PacienteService;

@Controller
public class PacienteController {

	@Autowired
	private PacienteService pacienteService;

	@RequestMapping(value="/paciente", method = RequestMethod.GET)
	public ModelAndView registration(){
		ModelAndView modelAndView = new ModelAndView();
		Paciente paciente = new Paciente();
		modelAndView.addObject("paciente", paciente);
		modelAndView.setViewName("admin/layout_paciente");
		return modelAndView;
	}

	@RequestMapping(value="/calendario", method = RequestMethod.GET)
	public ModelAndView caledario(){
		ModelAndView modelAndView = new ModelAndView();
		//Paciente paciente = new Paciente();
		//modelAndView.addObject("paciente", paciente);
		modelAndView.setViewName("admin/calendario");
		return modelAndView;
	}	
	
	@RequestMapping(value = "/paciente", method = RequestMethod.POST)
	public ModelAndView createNewPaciente(@Valid Paciente paciente, BindingResult bindingResult) {
		ModelAndView modelAndView = new ModelAndView();		
		//pacienteService.saveOrUpdate(paciente);
		modelAndView.addObject("successMessage", "Paciente has been registered successfully");
		//modelAndView.addObject("paciente", new Paciente());
		modelAndView.setViewName("admin/layout_paciente");
					
		return modelAndView;
	}
	
	@RequestMapping(value = "/pesquisa_paciente", method = RequestMethod.GET)
	public ModelAndView findPaciente(@Valid Paciente paciente, BindingResult bindingResult) {
		ModelAndView modelAndView = new ModelAndView();
		List<Paciente> pacientes = new ArrayList<>();

		pacientes = pacienteService.findByNomeOrCpfOrRg(paciente.getNome(), paciente.getCpf(), paciente.getRg());
				
		//modelAndView.addObject("successMessage", "Paciente foi registrado com sucesso");
		modelAndView.addObject("pacientes", pacientes);
		modelAndView.setViewName("admin/paciente/pesquisa_paciente");
					
		return modelAndView;
	}
	
	@RequestMapping(value = "/atualiza_paciente", method = RequestMethod.POST)
	public ModelAndView updatePaciente(@Valid Paciente paciente, BindingResult bindingResult) {
		ModelAndView modelAndView = new ModelAndView();		
		//pacienteService.saveOrUpdate(paciente);;
		modelAndView.addObject("successMessage", "Paciente foi atualizado com sucesso");
		//modelAndView.addObject("paciente", new Paciente());
		modelAndView.setViewName("paciente");
					
		return modelAndView;
	}
	
	
}
