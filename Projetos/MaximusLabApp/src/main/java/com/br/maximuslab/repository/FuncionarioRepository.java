package com.br.maximuslab.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.br.maximuslab.model.Funcionario;

@Repository("funcionarioRepository")
public interface FuncionarioRepository  extends JpaRepository<Funcionario, Long> {
	List<Funcionario> findByNomeOrCpfOrRg(String nome, String cpf, String rg);
}
