package com.br.maximuslab.model;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.constraints.Email;

@Entity
@Table(name = "Paciente")
public class Paciente extends AbstractEntity implements Serializable {

	private static final long serialVersionUID = 1L;

    @Id
    @GenericGenerator(name="kaugen" , strategy="increment")
    @GeneratedValue(generator="kaugen")
    @Column(name="ID")
	private long id;
    
    @Column(name = "CPF")
	private String cpf;
    
    @Column(name = "RG")
	private String rg;
        
    @Column(name = "NOME")
	private String nome;
    
    @Column(name = "TELEFONE")
	private String telefone;
    
    @Column(name = "CELULAR")
	private String celular;
    
    @Column(name = "DATA_NASC")
	private LocalDate dataNasc;
    
    @Column(name = "ENDERECO")
	private Endereco endereco;
    
    @Column(name = "RESPONSAVEL_PACIENTE")
	private ResponsavelPaciente responsavelPaciente;
    
	@Column(name = "EMAIL")
	@Email(message = "*Informe um e-mail válido")
	private String email;
	
    @Column(name = "DATA_CADASTRADO")
	private LocalDate dataCadastrado;

	public Long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getRg() {
		return rg;
	}

	public void setRg(String rg) {
		this.rg = rg;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public String getCelular() {
		return celular;
	}

	public void setCelular(String celular) {
		this.celular = celular;
	}

	public LocalDate getDataNasc() {
		return dataNasc;
	}

	public void setDataNasc(LocalDate dataNasc) {
		this.dataNasc = dataNasc;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	public ResponsavelPaciente getResponsavelPaciente() {
		return responsavelPaciente;
	}

	public void setResponsavelPaciente(ResponsavelPaciente responsavelPaciente) {
		this.responsavelPaciente = responsavelPaciente;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}   
	
}
