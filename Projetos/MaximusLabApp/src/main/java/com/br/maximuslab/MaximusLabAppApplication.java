package com.br.maximuslab;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MaximusLabAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(MaximusLabAppApplication.class, args);
	}
}
