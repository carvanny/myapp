package com.br.maximuslab.repository;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.br.maximuslab.model.Paciente;

@Repository("pacienteRepository")
public interface PacienteRepository extends JpaRepository<Paciente, Long > {	
	public List<Paciente> findByNomeOrCpfOrRg(String nome, String cpf, String rg);
	public List<Paciente> findByAll();
	//public List<Paciente> findByDataCadastrado(LocalDate dataInicio, LocalDate dataFim);
}
