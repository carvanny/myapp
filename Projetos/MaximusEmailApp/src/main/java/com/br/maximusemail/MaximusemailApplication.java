package com.br.maximusemail;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MaximusemailApplication {

	public static void main(String[] args) {
		SpringApplication.run(MaximusemailApplication.class, args);
	}
}
