package com.br.maximusemail.service;

import java.io.IOException;
import java.util.Arrays;
import java.util.Date;
import java.util.Locale;

import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.InputStreamSource;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.thymeleaf.context.Context;

public class EmailService {

	/*
	* Send HTML mail with inline image
	*/
	@RequestMapping(value = "/sendMailWithInlineImage", method = RequestMethod.POST)
	public String sendMailWithInline(
	  @RequestParam("recipientName") final String recipientName,
	  @RequestParam("recipientEmail") final String recipientEmail,
	  @RequestParam("image") final MultipartFile image,
	  final Locale locale)
	  throws MessagingException, IOException {

	    this.emailService.sendMailWithInline(
	        recipientName, recipientEmail, image.getName(),
	        image.getBytes(), image.getContentType(), locale);
	    return "redirect:sent.html";

	}
	
	public void sendMailWithInline(
			  final String recipientName, final String recipientEmail, final String imageResourceName,
			  final byte[] imageBytes, final String imageContentType, final Locale locale)
			  throws MessagingException {

			    // Prepare the evaluation context
				final Context ctx = new Context(locale);
				ctx.setVariable("name", recipientName);
				ctx.setVariable("subscriptionDate", new Date());
				ctx.setVariable("hobbies", Arrays.asList("Cinema", "Sports", "Music"));
				ctx.setVariable("imageResourceName", imageResourceName); // so that we can reference it from HTML
				
				final String htmlContent = this.templateEngine.process("templates/email-inlineimage.html", ctx);

			    // Prepare message using a Spring helper
			    final MimeMessage mimeMessage = this.mailSender.createMimeMessage();
			    final MimeMessageHelper message =
			        new MimeMessageHelper(mimeMessage, true, "UTF-8"); // true = multipart
			    message.setSubject("Example HTML email with inline image");
			    message.setFrom("thymeleaf@example.com");
			    message.setTo(recipientEmail);

			    // Create the HTML body using Thymeleaf
			    final String htmlContent = this.templateEngine.process("email-inlineimage.html", ctx);
			    message.setText(htmlContent, true); // true = isHtml

			    // Add the inline image, referenced from the HTML code as "cid:${imageResourceName}"
			    final InputStreamSource imageSource = new ByteArrayResource(imageBytes);
			    message.addInline(imageResourceName, imageSource, imageContentType);

			    // Send mail
			    this.mailSender.send(mimeMessage);

			}	
}
