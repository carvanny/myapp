$(function() {
    $('#side-menu').metisMenu();

    $('#trocaSenhaModal').on('shown.bs.modal', function () {
        $('#trocaSenhaFormSenhaAtual').focus();
    });
});

//Loads the correct sidebar on window load,
//collapses the sidebar on window resize.
// Sets the min-height of #page-wrapper to window size
$(function() {
    $(window).bind("load resize", function() {
        topOffset = 50;
        width = (this.window.innerWidth > 0) ? this.window.innerWidth : this.screen.width;
        if (width < 768) {
            $('div.navbar-collapse').addClass('collapse');
            topOffset = 100; // 2-row-menu
        } else {
            $('div.navbar-collapse').removeClass('collapse');
        }

        height = ((this.window.innerHeight > 0) ? this.window.innerHeight : this.screen.height) - 1;
        height = height - topOffset;
        if (height < 1) height = 1;
        if (height > topOffset) {
            $("#page-wrapper").css("min-height", (height) + "px");
        }
    });

    var url = window.location;
    var element = $('ul.nav a').filter(function() {
        return this.href == url || url.href.indexOf(this.href) == 0;
    }).addClass('active').parent().parent().addClass('in').parent();
    if (element.is('li')) {
        element.addClass('active');
    }
});

// muda o cursor do mouse para exibir a maozinha sobre as trs das tabelas
$(document).ready(function(){
	$('.trClick').click(function () {
		 window.location = $(this).attr("href");
    });
});

//contador de caracteres restantes no preenchimento do objeto
$(function(){

	$(".maxlength").keyup(function(event){

		// pega o span onde esta a quantidade maxima de caracteres.
		var target	= $("#content-countdown");
		
		// pega a quantidade maxima permitida pelo atributo title.
		var max		= target.attr('title');

		// tamanho da string dentro da textarea.
		var len 	= $(this).val().length;
		
		// quantidade de caracteres restantes dentro da textarea.
		var remain	= max - len;

		// caso a quantidade dentro da textarea seja maior que a quantidade maxima.
		if(len > max)
		{
			// mantem apenas os caracteres ate o tamanho maximo.
			var val = $(this).val();
			$(this).val(val.substr(0, max));
			
			// seta o restante para 0.
			remain = 0;
		}
		// atualiza a quantidade de caracteres restantes.
		target.html(remain);
	});
	
	$("input[type='tel']")
	.bind('input', function(event){
		$(this).val($(this).val().replace(/[^0-9() -]+/g, ''));
	})
	.on("keyup", function(event) {
	    var actual = $(this).val().substr(0); // string atual com mascara
	    var target = (event.currentTarget) ? event.currentTarget : event.srcElement; // objeto atual
	    var phone = target.value.replace(/\D/g, ''); // apenas numeros
	    if (event.keyCode == 46 || event.keyCode == 37 || event.keyCode == 39 || event.keyCode == 36 || event.keyCode == 35) { 
	    	// tecla 46 delete (apenas remove os caracteres)
	    	// teclas 37 <- e 39 -> (apenas move o cursor nos caracteres)
	    	// teclas 36 home e 35 end (move o cursor para o inicio ou o final do campo)
	    } else {
	    	// valida o formato atual do campo quando a tecla pressionada for diferente da tecla backspace
		    var value = '';
	    	if (phone.length > 0 && phone.length < 3){
	    		value += '(' + phone;
	    	} else if (phone.length > 2) {
	    		value += '(' + phone.substr(0, 2) + ') ';
	    		value += phone.substr(2, 4);
    			if (phone.length > 6) {
	    			if (phone.length < 11) {
	    				value += '-' + phone.substr(6, 10);
	    			} else {
	    				value += phone.substr(6, 1) + '-';
	    				value += phone.substr(7, 11);
	    			}
    			}
		    }
	    	if (value != '') {
	    		$(this).val(value);  
	    	}
    	}
	})
	.focus(function(event){
	    var target, actual, element;
	    target = (event.currentTarget) ? event.currentTarget : event.srcElement;
	    actual = target.value.replace(/\D/g, '');
	    element = $(target);
	    element.unmask();
	    var value = '';
	    if (actual.length > 0){
	    	value += '(' + actual.substr(0, 2) + ') ';
	    	if (actual.length == 11){
	    		value += actual.substr(2, 5) + '-';
	    		value += actual.substr(7, 9);
	    	} else {
	    		value += actual.substr(2, 4) + '-';
	    		value += actual.substr(6, 8);
	    	}
	    }
		$(this).val(value);  	    
	})
	.focusout(function(event) {
	    var target, phone, element;
	    target = (event.currentTarget) ? event.currentTarget : event.srcElement;
	    phone = target.value.replace(/\D/g, '');
	    element = $(target);
	    element.unmask();
	    if (phone.length > 10) {
	        element.mask("(99) 99999-999?9");
	    } else {
	        element.mask("(99) 9999-9999?9");  
	    }
	});
});

// mascara para telefone e celular
jQuery(document).ready(function() {
    // Mascara Telefone
	jQuery('input[type=tel]').focusout(function(){
        var phone, element;
        element = $(this);
        element.unmask();
        phone = element.val().replace(/\D/g, '');
        if(phone.length > 10) {
            element.mask("(99) 99999-999?9");
        } else {
            element.mask("(99) 9999-9999?9");
        }
    }).trigger('focusout');
    // Mascara RG
    jQuery("#rg").mask("99.999.999-*");
    // Mascara CPF
    jQuery("#cpf").mask("999.999.999-99");
});(jQuery);

// exibe as mensagens de sucesso ou de erro da aplicacao
function showDialogMessage(titulo, texto, classe) {
	if (titulo != '' && titulo != 'null' && texto != '' && texto != 'null' && classe != '') {
		bootbox.dialog({
			title: titulo,
			message: texto,
			buttons: {
			    success: {
			      label: 'Fechar',
			      className: classe
			    }
			}
		});
		setTimeout("bootbox.dialog()", 5000);
	}
}

// chama uma determinada acao (url via ajax)
function disparaAcao(url, parametro, destino) {
    if ($(parametro).val() != '') {
        url = url + '/' + $(parametro).val();
    }
    alert('url: ' + url);
	$(destino).load(url);
}

// atualiza o campo data para date ou para text e formata o valor do campo que será exibido
function reloadDate(campo) {
    if ($(campo).val() == ''){
    	$(campo).attr('type', 'text');
    } else if (!$(campo).val().isPrototypeOf(String)) {
    	$(campo).attr('type', 'text');
    	var dsplit = $(campo).val().split("-");
    	$(campo).val(dsplit[2] + '/' + dsplit[1] + '/' + dsplit[0]);
    }
}

$(document).ready(function(){
	var _options = {
	    highlight: function(element) {
	        var id_attr = "#" + $( element ).attr("id") + "1";
	        $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
	        $(id_attr).removeClass('glyphicon-ok').addClass('glyphicon-remove');         
	    },
	    unhighlight: function(element) {
	        var id_attr = "#" + $( element ).attr("id") + "1";
	        $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
	        $(id_attr).removeClass('glyphicon-remove').addClass('glyphicon-ok');         
	    },
	    errorElement: 'span',
	    errorClass: 'help-block',
	    errorPlacement: function(error, element) {
	        if(element.parent('.input-group').length) {
	            error.insertAfter(element.parent());
	        } else {
	            error.insertAfter(element);
	        }
	    },
	    ignore: ".ignore",
        	rules: {
        		hiddenRecaptcha: {
        			required: function () {
                    if (grecaptcha.getResponse() == '') {
                        return true;
                    } else {
                    	hiddenRecaptcha.value = grecaptcha.getResponse();
                        return false;
                    }
                }
            }
        },
        submitHandler: function (form) {
			// abre uma dialog box e exibe uma mensagem para o usuario aguardar, enquanto as informacoes estao sendo processadas
			bootbox.dialog({show: true, title: 'Por favor aguarde...', message: '<div class="text-center modalWait"><br/><br/><br/></div>', closeButton: false});
            form.submit();
        }
	};
	$("#loginForm").validate(_options);
	$("#faleConoscoForm").validate(_options);
	$("#esqueciSenhaForm").validate(_options);
	$("#enqueteForm").validate(_options);
	$("#trocaSenhaForm").validate(_options);
	$("#votoEnqueteForm").validate(_options);
 });

// update messages in portuguese
jQuery.extend(jQuery.validator.messages, {
    required: "Este campo &eacute; requerido.",
    remote: "Por favor, corrija este campo.",
    email: "Por favor, forne&ccedil;a um endere&ccedil;o eletr&ocirc;nico v&aacute;lido.",
    url: "Por favor, forne&ccedil;a uma URL v&aacute;lida.",
    date: "Por favor, forne&ccedil;a uma data v&aacute;lida.",
    dateISO: "Por favor, forne&ccedil;a uma data v&aacute;lida.",
    number: "Por favor, forne&ccedil;a um n&uacute;mero v&aacute;lido.",
    digits: "Por favor, forne&ccedil;a somente d&iacute;gitos.",
    creditcard: "Por favor, forne&ccedil;a um cart&atilde;o de cr&eacute;dito v&aacute;lido.",
    equalTo: "Por favor, forne&ccedil;a o mesmo valor novamente.",
    accept: "Por favor, forne&ccedil;a um valor com uma extens&atilde;o v&aacute;lida.",
    maxlength: jQuery.validator.format("Por favor, forne&ccedil;a apenas {0} caracteres."),
    minlength: jQuery.validator.format("Por favor, forne&ccedil;a pelo menos {0} caracteres."),
    rangelength: jQuery.validator.format("Por favor, forne&ccedil;a um valor entre {0} e {1} caracteres."),
    range: jQuery.validator.format("Por favor, forne&ccedil;a um valor entre {0} e {1}."),
    max: jQuery.validator.format("Por favor, forne&ccedil;a um valor menor ou igual a {0}."),
    min: jQuery.validator.format("Por favor, forne&ccedil;a um valor maior ou igual a {0}.")
});
