package br.com.maximuslab.configuration;

import java.util.Properties;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

@Configuration
@PropertySource(value = { "classpath:mail.properties" })
public class MailConfig {

	@Autowired
	private Environment environment;

	@Bean
	public JavaMailSender mailSender() {
		JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
		mailSender.setDefaultEncoding("UTF-8");
		mailSender.setHost(environment.getRequiredProperty("mail.server.host"));
		mailSender.setProtocol(environment.getRequiredProperty("mail.server.protocol"));
		mailSender.setUsername(environment.getRequiredProperty("mail.server.username"));
		mailSender.setPassword(environment.getRequiredProperty("mail.server.password"));
		mailSender.setJavaMailProperties(mailProperties());
		return mailSender;
	}
	 
    private Properties mailProperties() {
        Properties properties = new Properties();
        properties.put("mail.smtp.auth", environment.getRequiredProperty("mail.smtp.auth"));
        properties.put("mail.pop3.socketFactory.class", environment.getRequiredProperty("mail.pop3.socketFactory.class"));
        properties.put("mail.smtp.socketFactory.class", environment.getRequiredProperty("mail.smtp.socketFactory.class"));
        properties.put("mail.smtp.socketFactory.fallback", environment.getRequiredProperty("mail.smtp.socketFactory.fallback"));
        properties.put("mail.smtp.socketFactory.port", environment.getRequiredProperty("mail.smtp.socketFactory.port"));
        properties.put("mail.smtp.startssl.enable", environment.getRequiredProperty("mail.smtp.startssl.enable"));
        return properties;        
    }
}


