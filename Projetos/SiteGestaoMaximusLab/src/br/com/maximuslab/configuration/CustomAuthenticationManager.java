package br.com.maximuslab.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;

import br.com.maximuslab.entity.Usuario;

@Configuration
public class CustomAuthenticationManager implements AuthenticationManager {
	
	public Authentication authenticate(Usuario user) throws AuthenticationException {
		UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(user.getUsername(), user.getPassword(), user.getListGrantedAuthorities());
		return authenticate(authentication);
	}

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		SecurityContextHolder.createEmptyContext();
		SecurityContextHolder.getContext().setAuthentication(authentication);
        return authentication;
    }

}
