package br.com.maximuslab.controller;

import java.util.GregorianCalendar;
import java.util.Locale;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;

import br.com.maximuslab.entity.Mensagem;
import br.com.maximuslab.entity.Role;
import br.com.maximuslab.entity.TipoMensagem;
import br.com.maximuslab.entity.Usuario;
import br.com.maximuslab.service.UserService;
import br.com.maximuslab.util.Constantes;
import br.com.maximuslab.util.SecurityUtil;


/**
 * Application User Controller.
 */
@Controller
@Secured ({"ROLE_USER", "ROLE_MANAGER", "ROLE_ADMIN"})
public class UserController {

	private static Logger logger = Logger.getLogger(UserController.class);

	@Autowired
	private UserService userService;
	
    /** Home Page */
    @RequestMapping("/userIndex")
    public String userIndex(HttpSession session, Usuario user, Mensagem mensagem) {
    	String page = Constantes.INDEX;
    	user = (Usuario) session.getAttribute(Constantes.USER_SESSION);
		if (user != null && user.getCurrentRole() != null) {
			page = getUserPage(user);
		} else {
			return Constantes.REDIRECT_SESSAO_EXPIRADA;
		}
        return page;
    }
    
    @RequestMapping("/redirectUser")
    @Secured ({"ROLE_USER", "ROLE_MANAGER", "ROLE_ADMIN"})
	public String redirectUser(HttpSession session, Usuario user, Mensagem mensagem, Model model) {
    	user = (Usuario) session.getAttribute(Constantes.USER_SESSION);
		if (user != null && !user.getRoles().isEmpty()) {
			for (Role role : user.getRoles()) {
				if (Constantes.ROLE_USER.equals(role.getDescription())) {
					user.setCurrentRole(role);
					logger.info("| Perfil do usuario selecionado > " + role.getDescription() + " |");
					break;
				}
			}
		} else {
			return Constantes.REDIRECT_SESSAO_EXPIRADA;
		}
		return Constantes.USER_INDEX;
	}
    
    @RequestMapping("/redirectManager")
    @Secured ({"ROLE_MANAGER", "ROLE_ADMIN"})
	public String redirectManager(HttpSession session, Usuario user, Mensagem mensagem, Model model) {
    	user = (Usuario) session.getAttribute(Constantes.USER_SESSION);
		if (user != null && !user.getRoles().isEmpty()) {
			Role roleAtual = null;
			for (Role role : user.getRoles()) {
				if (Constantes.ROLE_MANAGER.equals(role.getDescription())) {
					user.setCurrentRole(role);
					roleAtual = role;
					logger.info("| Perfil do usuario selecionado > " + role.getDescription() + " |");
					break;
				}
			}
			if (roleAtual == null) return redirectUser(session, user, mensagem, model);
		} else {
			return Constantes.REDIRECT_SESSAO_EXPIRADA;
		}
		return Constantes.MANAGER_INDEX;
	}    
    
    @RequestMapping("/redirectAdmin")
    @Secured ({"ROLE_ADMIN"})
	public String redirectAdmin(HttpSession session, Usuario user, Mensagem mensagem, Model model) {
    	user = (Usuario) session.getAttribute(Constantes.USER_SESSION);
		if (user != null && !user.getRoles().isEmpty()) {
			Role roleAtual = null;
			for (Role role : user.getRoles()) {
				if (Constantes.ROLE_ADMIN.equals(role.getDescription())) {
					user.setCurrentRole(role);
					roleAtual = role;
					logger.info("| Perfil do usuario selecionado > " + role.getDescription() + " |");
					break;
				}
			}
			if (roleAtual == null) return redirectManager(session, user, mensagem, model);
		} else {
			return Constantes.REDIRECT_SESSAO_EXPIRADA;
		}
		return Constantes.ADMIN_INDEX;
	}    
    
    private String getUserPage(Usuario user){
    	String page = null;
		if (Constantes.ROLE_USER.equals(user.getCurrentRole().getDescription())) {
			page = Constantes.USER_INDEX;
		} else if (Constantes.ROLE_MANAGER.equals(user.getCurrentRole().getDescription())) {
			page = Constantes.MANAGER_INDEX;
		} else if (Constantes.ROLE_ADMIN.equals(user.getCurrentRole().getDescription())) {
			page = Constantes.ADMIN_INDEX;
		}
		return page;
    }
	
    @RequestMapping("/trocaSenha")
    public String trocaSenha(HttpSession session, final Usuario user, final Mensagem mensagem, final Model model, final Locale locale) {
    	Usuario userSession = (Usuario) session.getAttribute(Constantes.USER_SESSION);
    	if (trocaSenhaValidation(user)) {
    		try {
    			Usuario userBD = userService.search(userSession.getEmail());
    			if (!userBD.getPassword().equals(SecurityUtil.encryptPassword(user.getPassword()))){
    				// a senha atual n�o confere
    		    	mensagem.setTextoModal(Constantes.getBundle("troca.senha.senha.atual.invalida.mensagem.texto.erro"));
    		    	model.addAttribute(Constantes.PARAMETRO_EXIBE_TROCA_SENHA_MODAL , Constantes.VALOR_PARAMETRO_EXIBE_MODAL);
    				logger.error("| erro de alteracao de senha - a senha atual n�o confere com a senha informada |");
    			} else if (user.getPassword().equals(user.getNovaSenha())){
    				// a nova senha deve ser diferente da senha atual
    		    	mensagem.setTextoModal(Constantes.getBundle("troca.senha.nova.senha.identica.anterior.mensagem.texto.erro"));
    		    	model.addAttribute(Constantes.PARAMETRO_EXIBE_TROCA_SENHA_MODAL , Constantes.VALOR_PARAMETRO_EXIBE_MODAL);
    				logger.error("| erro de alteracao de senha - a nova senha e identica a senha atual |");
    			} else if (!user.getNovaSenha().equals(user.getConfirmacaoNovaSenha())){
    				// a nova senha deve ser igual a confirmacao da nova senha
    		    	mensagem.setTextoModal(Constantes.getBundle("troca.senha.nova.senha.diferente.confirmacao.mensagem.texto.erro"));
    		    	model.addAttribute(Constantes.PARAMETRO_EXIBE_TROCA_SENHA_MODAL , Constantes.VALOR_PARAMETRO_EXIBE_MODAL);
    				logger.error("| erro de alteracao de senha - a nova senha e difetente da confirmacao da senha |");
    			} else {
					userBD.setPassword(SecurityUtil.encryptPassword(user.getNovaSenha()));
					userBD.setDataAlteracao(new GregorianCalendar());
					userBD.setNovaSenha(Constantes.STRING_VALOR_CAMPO_FALSO);
					userService.update(userBD);
					userBD.setPassword(user.getNovaSenha());
					userService.enviaEmailTrocaSenhaUsuario(userBD, locale);
					mensagem.setTitulo(Constantes.getBundle("troca.senha.mensagem.titulo.sucesso"));
		        	mensagem.setTexto(Constantes.getBundle("troca.senha.mensagem.texto.sucesso"));
		        	mensagem.setTipoMensagem(TipoMensagem.SUCESSO);
					logger.info("| troca de senha do usuario realizada com sucesso -> " + userSession.getEmail() + " |");
    			}
			} catch (IllegalArgumentException ex) {
				// atualiza as informacoes que serao exibidas na mensagem de erro para o usuario
				mensagem.setTextoModal(Constantes.getBundle("geracao.nova.senha.email.invalido.mensagem.erro"));
				model.addAttribute(Constantes.PARAMETRO_EXIBE_TROCA_SENHA_MODAL , Constantes.VALOR_PARAMETRO_EXIBE_MODAL);
				logger.error(ex, ex.getCause());
			} catch (Exception e) {
	    		// atualiza as informacoes que serao exibidas na mensagem de erro para o usuario
				mensagem.setTitulo(Constantes.getBundle("geracao.nova.senha.mensagem.titulo.erro"));
	        	mensagem.setTexto(Constantes.getBundle("geracao.nova.senha.mensagem.texto.erro"));
	        	mensagem.setTipoMensagem(TipoMensagem.ERRO);
	        	logger.error(e, e.getCause());
			}    	
		} else {
			// atualiza as informacoes que serao exibidas na mensagem de erro para o usuario
	    	mensagem.setTextoModal(Constantes.getBundle("mensagem.erro.validacao.campos.obrigatorios.nao.informados"));
	    	model.addAttribute(Constantes.PARAMETRO_EXIBE_TROCA_SENHA_MODAL , Constantes.VALOR_PARAMETRO_EXIBE_MODAL);
			logger.error("| erro de alteracao de senha - campos obrigatorios nao informados |");
		}  	
		
    	return getUserPage(userSession);
    }        

	private boolean trocaSenhaValidation(Usuario user) {
		Boolean validation = false;
		if (user != null && (!StringUtils.isEmpty(user.getPassword()) && !StringUtils.isEmpty(user.getNovaSenha()) && !StringUtils.isEmpty(user.getConfirmacaoNovaSenha()))) {
			validation = true;
		}
		return validation;
	}
    
}