package br.com.maximuslab.controller;

import java.util.GregorianCalendar;
import java.util.Locale;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;

import br.com.maximuslab.configuration.CustomAuthenticationManager;
import br.com.maximuslab.entity.Mensagem;
import br.com.maximuslab.entity.Role;
import br.com.maximuslab.entity.TipoMensagem;
import br.com.maximuslab.entity.Usuario;
import br.com.maximuslab.service.UserService;
import br.com.maximuslab.util.Constantes;
import br.com.maximuslab.util.SecurityUtil;


/**
 * Application Home Controller.
 */
@Controller
public class HomeController {

	private static Logger logger = Logger.getLogger(HomeController.class);
	
	private static CustomAuthenticationManager authenticationManager = new CustomAuthenticationManager();

	@Autowired
	private UserService userService;

    /** Home Page */
    @RequestMapping("/")
    public String home(final Usuario user, final Mensagem mensagem, final Model model) {
        return Constantes.INDEX;
    }

    @RequestMapping("/index")
    public String index(final Usuario user, final Mensagem mensagem, final Model model) {
        return Constantes.INDEX;
    }

    @RequestMapping("/login")
    public String login(final Usuario user, final Mensagem mensagem, final Model model) {
    	limpaAtributosSessao(user);
    	model.addAttribute(Constantes.PARAMETRO_EXIBE_LOGIN_MODAL , Constantes.VALOR_PARAMETRO_EXIBE_MODAL);
        return Constantes.INDEX;
    }
    
    @RequestMapping("/efetuaLogin")
    public String efetuaLogin(HttpSession session, final Usuario user, final Mensagem mensagem, final Model model) {
    	String page = Constantes.INDEX;
    	Usuario usuario = null;
    	if (validation(user)) {
	    	try {
	    		// autenticando o usuario
	    		usuario = userService.login(user);
				if (usuario.getDataInativacao() != null) {
					logger.error("| erro de login - usuario inativo aplicacao -> " + usuario.getEmail() + " |");
		        	mensagem.setTextoModal(Constantes.getBundle("login.usuario.inativo.mensagem.texto.erro"));
		        	model.addAttribute(Constantes.PARAMETRO_EXIBE_LOGIN_MODAL , Constantes.VALOR_PARAMETRO_EXIBE_MODAL);
				} else {
					page = pageRedirect(usuario, mensagem, model);
					if (!Constantes.INDEX.equals(page)) {
						springAuthenticate(session, usuario);
						if (Constantes.STRING_VALOR_CAMPO_VERDADEIRO.equals(usuario.getNovaSenha())) {
							model.addAttribute(Constantes.PARAMETRO_EXIBE_TROCA_SENHA_MODAL , Constantes.VALOR_PARAMETRO_EXIBE_MODAL);
						}
						logger.info("| login do usuario realizado com sucesso -> " + usuario.getEmail() + " |");
					} else {
						// usuario possui algum problema com o perfil
						usuario = null;
					}
				}
			} catch (IllegalArgumentException ex) {
				// atualiza as informacoes que serao exibidas na mensagem de erro para o usuario
				mensagem.setTextoModal(Constantes.getBundle("login.usuario.credenciais.invalidas.mensagem.texto.erro"));
				model.addAttribute(Constantes.PARAMETRO_EXIBE_LOGIN_MODAL , Constantes.VALOR_PARAMETRO_EXIBE_MODAL);
				logger.error(ex, ex.getCause());
			} catch (Exception e) {
	    		// atualiza as informacoes que serao exibidas na mensagem de erro para o usuario
				mensagem.setTitulo(Constantes.getBundle("login.usuario.mensagem.titulo.erro"));
	        	mensagem.setTexto(Constantes.getBundle("login.usuario.mensagem.texto.erro"));
	        	mensagem.setTipoMensagem(TipoMensagem.ERRO);
	        	logger.error(e, e.getCause());
			}
		} else {
    		// atualiza as informacoes que serao exibidas na mensagem de erro para o usuario
        	mensagem.setTextoModal(Constantes.getBundle("login.usuario.credenciais.nao.informadas.mensagem.texto.erro"));
        	model.addAttribute(Constantes.PARAMETRO_EXIBE_LOGIN_MODAL , Constantes.VALOR_PARAMETRO_EXIBE_MODAL);
			logger.error("| erro de login - credenciais nao informadas |");
		}  	
    	return page;
    }    
    
	private String pageRedirect(Usuario user, Mensagem mensagem, Model model) {
		String page = Constantes.INDEX;
		if (!user.getRoles().isEmpty()) {
			if (user.getRoles().size() > 1) {
				page = Constantes.MANAGER_INDEX;
				for (Role role : user.getRoles()) {
					if (Constantes.ROLE_MANAGER.equals(role.getDescription())) {
						user.setCurrentRole(role);
						break;
					}
				}
				logger.info("| Usuario com multiplos perfis > ROLE_MANAGER |");
			} else {
				Role role = user.getRoles().get(0);
				user.setCurrentRole(role);
				logger.info("| Perfil do usuario > " + role.getDescription() + " |");
				if (Constantes.ROLE_USER.equals(role.getDescription())) {
					page = Constantes.USER_INDEX;
				} else {
		    		// atualiza as informacoes que serao exibidas na mensagem de erro para o usuario
					mensagem.setTitulo(Constantes.getBundle("login.usuario.perfil.inexistente.mensagem.titulo.erro"));
		        	mensagem.setTexto(Constantes.getBundle("login.usuario.perfil.inexistente.mensagem.texto.erro"));
		        	mensagem.setTipoMensagem(TipoMensagem.ERRO);
		        	logger.error("| usuario sem perfil associado > " + user.getUsername() + " |");
				}
			}
		} else {
    		// atualiza as informacoes que serao exibidas na mensagem de erro para o usuario
			mensagem.setTitulo(Constantes.getBundle("login.usuario.perfil.invalido.mensagem.titulo.erro"));
        	mensagem.setTexto(Constantes.getBundle("login.usuario.perfil.invalido.mensagem.texto.erro"));
        	mensagem.setTipoMensagem(TipoMensagem.ERRO);
        	logger.error("| usuario com perfil invalido > " + user.getUsername() + " |");
		}
		return page;
	}

	public void springAuthenticate(HttpSession session, Usuario user) {
		authenticationManager.authenticate(user);
		session.setAttribute(Constantes.USER_SESSION, user);
	}
	
	private boolean validation(Usuario user) {
		Boolean validation = false;
		if (user != null && (!StringUtils.isEmpty(user.getEmail()) && !StringUtils.isEmpty(user.getPassword()))) {
			validation = true;
		}
		return validation;
	}

    @RequestMapping("/regeraSenha")
    public String regeraSenha(final Usuario user, final Mensagem mensagem, final Model model, final Locale locale) {
    	if (user != null && !StringUtils.isEmpty(user.getEmail())) {
    		try {
				Usuario userBD = userService.search(user.getEmail());
				String newPassword = SecurityUtil.passwordGenerate();
				userBD.setPassword(SecurityUtil.encryptPassword(newPassword));
				userBD.setDataAlteracao(new GregorianCalendar());
				userBD.setNovaSenha(Constantes.STRING_VALOR_CAMPO_VERDADEIRO);
				userService.update(userBD);
				userBD.setPassword(newPassword);
				userService.enviaEmailNovaSenhaUsuario(userBD, locale);
				mensagem.setTitulo(Constantes.getBundle("geracao.nova.senha.mensagem.titulo.sucesso"));
	        	mensagem.setTexto(Constantes.getBundle("geracao.nova.senha.mensagem.texto.sucesso"));
	        	mensagem.setTipoMensagem(TipoMensagem.SUCESSO);
				logger.info("| senha do usuario regerada com sucesso -> " + user.getEmail() + " |");
			} catch (IllegalArgumentException ex) {
				// atualiza as informacoes que serao exibidas na mensagem de erro para o usuario
				mensagem.setTextoModal(Constantes.getBundle("geracao.nova.senha.email.invalido.mensagem.erro"));
				model.addAttribute(Constantes.PARAMETRO_EXIBE_GERACAO_NOVA_SENHA_MODAL , Constantes.VALOR_PARAMETRO_EXIBE_MODAL);
				logger.error(ex, ex.getCause());
			} catch (Exception e) {
	    		// atualiza as informacoes que serao exibidas na mensagem de erro para o usuario
				mensagem.setTitulo(Constantes.getBundle("geracao.nova.senha.mensagem.titulo.erro"));
	        	mensagem.setTexto(Constantes.getBundle("geracao.nova.senha.mensagem.texto.erro"));
	        	mensagem.setTipoMensagem(TipoMensagem.ERRO);
	        	logger.error(e, e.getCause());
			}    	
		} else {
			// atualiza as informacoes que serao exibidas na mensagem de erro para o usuario
	    	mensagem.setTextoModal(Constantes.getBundle("geracao.nova.senha.email.nao.informado.mensagem.texto.erro"));
	    	model.addAttribute(Constantes.PARAMETRO_EXIBE_GERACAO_NOVA_SENHA_MODAL , Constantes.VALOR_PARAMETRO_EXIBE_MODAL);
			logger.error("| erro de geracao nova senha - email nao informado |");
		}  	
		
    	return Constantes.INDEX;
    }        
    
    @RequestMapping("/cadastraUsuario")
    public String cadastraUsuario(final Usuario user, final Mensagem mensagem, final Model model, final Locale locale) {
    	try {
    		if (validaCadastro(user, mensagem, model)) {
				String newPassword = SecurityUtil.passwordGenerate();
				user.setPassword(SecurityUtil.encryptPassword(newPassword));
				user.setDataInclusao(new GregorianCalendar());
				user.setNovaSenha(Constantes.STRING_VALOR_CAMPO_VERDADEIRO);
				user.setDataInativacao(null);
				user.setUsuarioInativacao(null);				
				userService.create(user, userService.listRolesByDescription(Constantes.ROLE_USER));
				user.setPassword(newPassword);
				userService.enviaEmailNovoUsuario(user, locale);
				mensagem.setTitulo(Constantes.getBundle("cadastro.novo.usuario.mensagem.titulo.sucesso"));
	        	mensagem.setTexto(Constantes.getBundle("cadastro.novo.usuario.mensagem.texto.sucesso"));
	        	mensagem.setTipoMensagem(TipoMensagem.SUCESSO);
				logger.info("| usuario cadastrado com sucesso -> " + user.getEmail() + " |");
			}
		} catch (Exception e) {
    		// atualiza as informacoes que serao exibidas na mensagem de erro para o usuario
			mensagem.setTitulo(Constantes.getBundle("cadastro.novo.usuario.mensagem.titulo.erro"));
        	mensagem.setTexto(Constantes.getBundle("cadastro.novo.usuario.mensagem.texto.erro"));
        	mensagem.setTipoMensagem(TipoMensagem.ERRO);
        	logger.error(e, e.getCause());
		}
    	return Constantes.INDEX;
    }    

	private boolean validaCadastro(Usuario user, Mensagem mensagem, Model model) throws Exception {
		Boolean validation = true;
		if (user != null) {
			if (StringUtils.isEmpty(user.getNome()) || StringUtils.isEmpty(user.getSobrenome()) || user.getDataNascimento() == null || 
				StringUtils.isEmpty(user.getEmail()) || StringUtils.isEmpty(user.getUsername())) {
	    		// atualiza as informacoes que serao exibidas na mensagem de erro para o usuario
	        	mensagem.setTextoModal(Constantes.getBundle("mensagem.erro.validacao.campos.obrigatorios.nao.informados"));
	        	model.addAttribute(Constantes.PARAMETRO_EXIBE_CADASTRO_USUARIO_MODAL , Constantes.VALOR_PARAMETRO_EXIBE_MODAL);
				logger.error("| erro de validacao no cadastro de usuario - campos obrigatorios nao informados |");
				validation = false;
			}
			if (!StringUtils.isEmpty(user.getEmail())) {
				user.setEmail(user.getEmail().toLowerCase());
				if (userService.existeEmail(user)) {
					validation = false;
					// atualiza as informacoes que serao exibidas na mensagem de erro para o usuario
		        	mensagem.setTextoModal(Constantes.getBundle("cadastro.novo.usuario.email.ja.existe"));
		        	model.addAttribute(Constantes.PARAMETRO_EXIBE_CADASTRO_USUARIO_MODAL , Constantes.VALOR_PARAMETRO_EXIBE_MODAL);
					logger.error("| erro de validacao no cadastro de usuario - o email informado ja existe no BD |");
				}
			}
			if (!StringUtils.isEmpty(user.getUsername())) {
				user.setUsername(user.getUsername().toLowerCase());
				if (userService.existeUsername(user)) {
					validation = false;
					// atualiza as informacoes que serao exibidas na mensagem de erro para o usuario
		        	mensagem.setTextoModal(Constantes.getBundle("cadastro.novo.usuario.username.ja.existe"));
		        	model.addAttribute(Constantes.PARAMETRO_EXIBE_CADASTRO_USUARIO_MODAL , Constantes.VALOR_PARAMETRO_EXIBE_MODAL);
					logger.error("| erro de validacao no cadastro de usuario - o username informado ja existe no BD |");
				}
			}
		} else {
			// atualiza as informacoes que serao exibidas na mensagem de erro para o usuario
			mensagem.setTitulo(Constantes.getBundle("cadastro.novo.usuario.mensagem.titulo.erro"));
        	mensagem.setTexto(Constantes.getBundle("cadastro.novo.usuario.mensagem.texto.erro"));
        	mensagem.setTipoMensagem(TipoMensagem.ERRO);
        	model.addAttribute(Constantes.PARAMETRO_EXIBE_CADASTRO_USUARIO_MODAL , Constantes.VALOR_PARAMETRO_EXIBE_MODAL);
        	logger.error("| erro de validacao no cadastro de usuario - o objeto user esta nulo |");
		}
		return validation;
	}

    @RequestMapping("/sessaoExpirada")
    public String sessaoExpirada(HttpSession session, final Usuario user, final Mensagem mensagem, final Model model) {
    	logger.info("| usuario nao autenticado ou sessao expirada |");
    	mensagem.setTextoModal(Constantes.getBundle("sessao.expirada.usuario.nao.autenticado.mensagem.texto.erro"));
    	model.addAttribute(Constantes.PARAMETRO_EXIBE_LOGIN_MODAL , Constantes.VALOR_PARAMETRO_EXIBE_MODAL);
        return encerraSessao(session);
    }
    
    @RequestMapping("/acessoNegado")
    public String acessoNegado(HttpSession session, final Usuario user, final Mensagem mensagem, final Model model) {
    	logger.info("| acesso negado - usuario sem permissao para acessar a requisicao solicitada |");
    	mensagem.setTextoModal(Constantes.getBundle("acesso.negado.usuario.nao.possui.permissao.mensagem.texto.erro"));
    	model.addAttribute(Constantes.PARAMETRO_EXIBE_LOGIN_MODAL , Constantes.VALOR_PARAMETRO_EXIBE_MODAL);
        return encerraSessao(session);
    }

    @RequestMapping("/logout")
    public String logout(HttpSession session, final Usuario user, final Mensagem mensagem, final Model model) {
		mensagem.setTitulo(Constantes.getBundle("logout.usuario.mensagem.titulo.sucesso"));
    	mensagem.setTexto(Constantes.getBundle("logout.usuario.mensagem.texto.sucesso"));
    	mensagem.setTipoMensagem(TipoMensagem.SUCESSO);
    	logger.info("| logout de usuario realizado com sucesso |");
        return encerraSessao(session);
    }
    
    private String encerraSessao(HttpSession session) {
    	session.removeAttribute(Constantes.USER_SESSION);
    	SecurityContextHolder.clearContext();
    	return Constantes.INDEX;
    }
    
    private void limpaAtributosSessao(Usuario user) {
    	user.reset();
    }
}