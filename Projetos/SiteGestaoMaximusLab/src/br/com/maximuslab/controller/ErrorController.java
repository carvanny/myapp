package br.com.maximuslab.controller;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import br.com.maximuslab.entity.Mensagem;
import br.com.maximuslab.entity.TipoMensagem;
import br.com.maximuslab.entity.Usuario;
import br.com.maximuslab.util.Constantes;


/**
 * Application Error Controller.
 */
@Controller
public class ErrorController {
	
    @RequestMapping("/error")
    public String erro(HttpSession session, final Usuario user, final Mensagem mensagem, final Model model) {
		mensagem.setTitulo(Constantes.getBundle("excecao.nao.tratada.problema.interno.mensagem.titulo.erro"));
    	mensagem.setTexto(Constantes.getBundle("excecao.nao.tratada.problema.interno.mensagem.texto.erro"));
    	mensagem.setTipoMensagem(TipoMensagem.ERRO);
        return sessionValidate(session);
    }

    @RequestMapping("/404")
    public String redirect404(HttpSession session, final Usuario user, final Mensagem mensagem, final Model model) {
		mensagem.setTitulo(Constantes.getBundle("pagina.nao.encontrada.mensagem.titulo.erro"));
    	mensagem.setTexto(Constantes.getBundle("pagina.nao.encontrada.mensagem.texto.erro"));
    	mensagem.setTipoMensagem(TipoMensagem.ERRO);
        return sessionValidate(session);
    }
    
    private String sessionValidate(HttpSession session) {
    	boolean valid = false;
    	Usuario user = (Usuario) session.getAttribute(Constantes.USER_SESSION);
    	if (user != null) {
    		valid = true;
    	}
    	return (valid ? Constantes.REDIRECT_USER_INDEX : Constantes.INDEX);
    }
}
