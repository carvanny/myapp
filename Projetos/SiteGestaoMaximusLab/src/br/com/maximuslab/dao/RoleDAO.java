package br.com.maximuslab.dao;

import java.io.Serializable;
import java.util.List;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;

import br.com.maximuslab.entity.Role;

@Repository
public class RoleDAO extends AbstractGenericDAO<Role> implements Serializable {
	private static final long serialVersionUID = 6101614003648606841L;

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Role getRoleByDescription(String description) {
		CriteriaQuery criteriaQuery = getCriteriaQuery();
		Root root = getRoot(criteriaQuery);
		criteriaQuery.select(root);

		criteriaQuery.where(getEntityManager().getCriteriaBuilder().equal(root.get("description"), description));
		
		List list = getEntityManager().createQuery(criteriaQuery).getResultList();

		Role role = null;
		if (list != null && list.size() > 0) {
			role = (Role) list.get(0);
		}
		return role;
	}
}
