package br.com.maximuslab.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.maximuslab.entity.Role;
import br.com.maximuslab.entity.Usuario;

@Repository
@Transactional
public class UserDAO extends AbstractGenericDAO<Usuario> implements Serializable {
	private static final long serialVersionUID = 6101614003648606841L;

	@SuppressWarnings({"rawtypes", "unchecked"})
	public Usuario login(String emailUsername, String password) {
		CriteriaQuery criteriaQuery = getCriteriaQuery();
		Root root = getRoot(criteriaQuery);
		criteriaQuery.select(root);

		Predicate predicateUserName = getEntityManager().getCriteriaBuilder().equal(root.get("username"), emailUsername);
		Predicate predicateEmail 	= getEntityManager().getCriteriaBuilder().equal(root.get("email"), emailUsername);
		Predicate predicatePassword = getEntityManager().getCriteriaBuilder().equal(root.get("password"), password);
		
		Predicate predicateUserPassword  = getEntityManager().getCriteriaBuilder().and(predicateUserName, predicatePassword);
		Predicate predicateEmailPassword = getEntityManager().getCriteriaBuilder().and(predicateEmail, predicatePassword);
		criteriaQuery.where(getEntityManager().getCriteriaBuilder().or(predicateUserPassword, predicateEmailPassword));
		
		List list = getEntityManager().createQuery(criteriaQuery).getResultList();
		
		Usuario user = null;
		if (list != null && list.size() > 0) {
			user = (Usuario) list.get(0);
		}
		return user;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Usuario searchEmail(String email) {
		CriteriaQuery criteriaQuery = getCriteriaQuery();
		Root root = getRoot(criteriaQuery);
		criteriaQuery.select(root);

		criteriaQuery.where(getEntityManager().getCriteriaBuilder().equal(root.get("email"), email));
		
		List list = getEntityManager().createQuery(criteriaQuery).getResultList();

		if (list == null || list.isEmpty() || list.size() > 1) {
			return null;
		}
		return (Usuario) list.get(0);
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Usuario existeEmail(String email) {
		CriteriaQuery criteriaQuery = getCriteriaQuery();
		Root root = getRoot(criteriaQuery);
		criteriaQuery.select(root);

		criteriaQuery.where(getEntityManager().getCriteriaBuilder().equal(root.get("email"), email));
		
		List list = getEntityManager().createQuery(criteriaQuery).getResultList();

		Usuario user = null;
		if (list != null && list.size() > 0) {
			user = (Usuario) list.get(0);
		}
		return user;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Usuario existeUsername(String username) {
		CriteriaQuery criteriaQuery = getCriteriaQuery();
		Root root = getRoot(criteriaQuery);
		criteriaQuery.select(root);

		criteriaQuery.where(getEntityManager().getCriteriaBuilder().equal(root.get("username"), username));
		
		List list = getEntityManager().createQuery(criteriaQuery).getResultList();

		Usuario user = null;
		if (list != null && list.size() > 0) {
			user = (Usuario) list.get(0);
		}
		return user;
	}

	public List<Usuario> findUsersByRole(Role role) {
		List<Usuario> usersRole = new ArrayList<Usuario>();
		List<Usuario> listAll = findAll();
		for (Usuario user: listAll) {
			if (!user.getRoles().isEmpty() && user.getRoles().size() == 1) {
				Role userRole = user.getRoles().get(0);
				if (userRole.getId().equals(role.getId())) {
					usersRole.add(user);
				}
			}
		}
		return usersRole;
	}

}
