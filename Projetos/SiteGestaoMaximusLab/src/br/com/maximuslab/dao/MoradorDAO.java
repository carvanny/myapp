package br.com.maximuslab.dao;

import java.io.Serializable;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.maximuslab.entity.Morador;

@Repository
@Transactional
public class MoradorDAO extends AbstractGenericDAO<Morador> implements Serializable {
	private static final long serialVersionUID = 6101614003648606841L;

}
