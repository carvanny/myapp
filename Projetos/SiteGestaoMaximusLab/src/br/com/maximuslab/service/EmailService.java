package br.com.maximuslab.service;

import java.io.IOException;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.InputStreamSource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import br.com.maximuslab.entity.Email;
import br.com.maximuslab.util.Constantes;

@Service
public class EmailService extends Constantes {

    @Autowired 
    private JavaMailSender mailSender;
    
    @Autowired 
    private TemplateEngine templateEngine;

    public void setMailSender(JavaMailSender mailSender) {
        this.mailSender = mailSender;
    }

    /* 
     * Send HTML mail (simple) 
     */
    public void sendSimpleMail(final Context context, final Email email) throws MessagingException {
        
        // Prepare message using a Spring helper
        final MimeMessage mimeMessage = this.mailSender.createMimeMessage();
        final MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage, "UTF-8");
        
        // Update message properties and HTML Content 
        updateMessageProperties(context, mimeMessageHelper, email);
        
        // Send email
        this.mailSender.send(mimeMessage);

    }

    /* 
     * Send HTML mail with inline image
     */
    public void sendMailWithInline(final Context context, final Email email) throws MessagingException, IOException {
        
        // Prepare message using a Spring helper
        final MimeMessage mimeMessage = this.mailSender.createMimeMessage();
        final MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage, true /* multipart */, "UTF-8");

        // Update message properties and HTML Content 
        updateMessageProperties(context, mimeMessageHelper, email);
        
        // Add the inline image, referenced from the HTML code as "cid:${imageResourceName}"
        final InputStreamSource imageSource = new ByteArrayResource(email.getByteArray());
        mimeMessageHelper.addInline(email.getFileName(), imageSource, email.getContentType());
        
        // Send mail
        this.mailSender.send(mimeMessage);
        
    }

    /* 
     * Send HTML mail with attachment. 
     */
    public void sendMailWithAttachment(final Context context, final Email email) throws MessagingException, IOException {
        
        // Prepare message using a Spring helper
        final MimeMessage mimeMessage = this.mailSender.createMimeMessage();
        final MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage, true /* multipart */, "UTF-8");

        // Update message properties and HTML Content 
        updateMessageProperties(context, mimeMessageHelper, email);
        
        // Add the attachment
        final InputStreamSource attachmentSource = new ByteArrayResource(email.getByteArray());
        mimeMessageHelper.addAttachment(email.getFileName(), attachmentSource, email.getContentType());
        
        // Send mail
        this.mailSender.send(mimeMessage);
        
    }

    /*
     * Update message properties and HTML Content
     */
    private void updateMessageProperties(Context context, MimeMessageHelper mimeMessageHelper, Email email) throws MessagingException {
    	
    	// Update message properties
    	mimeMessageHelper.setSubject(email.getSubject());
    	mimeMessageHelper.setFrom(email.getFromEmail());
    	mimeMessageHelper.setTo(email.getRecipientEmail());

        // Create the HTML body using Thymeleaf
        final String htmlContent = this.templateEngine.process(email.getPageHTML(), context);
        mimeMessageHelper.setText(htmlContent, true);

    }

}
