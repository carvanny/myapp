package br.com.maximuslab.service;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.thymeleaf.context.Context;

import br.com.maximuslab.dao.RoleDAO;
import br.com.maximuslab.dao.UserDAO;
import br.com.maximuslab.entity.Email;
import br.com.maximuslab.entity.Role;
import br.com.maximuslab.entity.Usuario;
import br.com.maximuslab.util.Constantes;
import br.com.maximuslab.util.FileUtil;
import br.com.maximuslab.util.SecurityUtil;

@Service
@Transactional
public class UserService {
	private static Logger logger = Logger.getLogger(UserService.class);

    @Autowired
    private EmailService emailService;
    
	@Autowired
	private UserDAO dao;

	@Autowired
	private RoleDAO roleDao;

	public Usuario login(Usuario user) throws IllegalArgumentException, Exception {
		
		Usuario userAuthenticated = dao.login(user.getEmail(), SecurityUtil.encryptPassword(user.getPassword()));
		
		if (userAuthenticated == null) {
			String msg = "usuario nao autenticado > " + user.getEmail();
			logger.info(msg);
			throw new IllegalArgumentException(msg);
		}
		return userAuthenticated;
	}
	
	public Usuario search(String email) throws IllegalArgumentException, Exception {
		Usuario user = dao.searchEmail(email);
		
		if (user == null) {
			String msg = "email nao encontrado no banco de dados > " + email;
			logger.info(msg);
			throw new IllegalArgumentException(msg);
		}
		return user;
	}

	public Boolean existeEmail(Usuario user) throws Exception {
		Usuario userLocalizado = dao.existeEmail(user.getEmail());
		Boolean encontrou = false; 
		
		if (userLocalizado != null) {
			encontrou = true; 
		}
		return encontrou;
	}

	public Boolean existeUsername(Usuario user) throws Exception {
		Usuario userLocalizado = dao.existeUsername(user.getUsername());
		Boolean encontrou = false;
		
		if (userLocalizado != null) {
			encontrou = true; 
		}
		return encontrou;
	}

	public void enviaEmailNovaSenhaUsuario(Usuario user, Locale locale) throws Exception {
		final Context context = new Context(locale);
        context.setVariable("nome", user.getNome());
        context.setVariable("usuario", user.getUsername());
        context.setVariable("email", user.getEmail());
        context.setVariable("senha", user.getPassword());
		final Email email = new Email();
        email.setSubject(Constantes.getBundle("geracao.nova.senha.subject.confirmacao"));
        email.setFromEmail(Constantes.getBundle("email.padrao.remetente.interno"));
        email.setRecipientEmail(user.getEmail());
        email.setPageHTML(Constantes.getBundle("pagina.email.confirmacao.geracao.nova.senha"));
        email.setFileName(Constantes.getBundle("nome.imagem.logo.email.site"));
        email.setContentType(Constantes.getBundle("content.type.imagem.logo"));
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream(email.getFileName());
        email.setByteArray(FileUtil.getByteArray(inputStream));
        // referencia utilizada apenas para o envio de arquivo in-line, para arquivo em anexo nao ha necessidade
        context.setVariable("imageResourceName", email.getFileName());
		emailService.sendMailWithInline(context, email);
	}
	
	public void enviaEmailTrocaSenhaUsuario(Usuario user, Locale locale) throws Exception {
		final Context context = new Context(locale);
        context.setVariable("nome", user.getNome());
        context.setVariable("usuario", user.getUsername());
        context.setVariable("email", user.getEmail());
        context.setVariable("senha", user.getPassword());
		final Email email = new Email();
        email.setSubject(Constantes.getBundle("geracao.nova.senha.subject.confirmacao"));
        email.setFromEmail(Constantes.getBundle("email.padrao.remetente.interno"));
        email.setRecipientEmail(user.getEmail());
        email.setPageHTML(Constantes.getBundle("pagina.email.confirmacao.geracao.nova.senha"));
        email.setFileName(Constantes.getBundle("nome.imagem.logo.email.site"));
        email.setContentType(Constantes.getBundle("content.type.imagem.logo"));
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream(email.getFileName());
        email.setByteArray(FileUtil.getByteArray(inputStream));
        // referencia utilizada apenas para o envio de arquivo in-line, para arquivo em anexo nao ha necessidade
        context.setVariable("imageResourceName", email.getFileName());
		emailService.sendMailWithInline(context, email);
	}

	public void enviaEmailNovoUsuario(Usuario user, Locale locale) throws Exception {
		final Context context = new Context(locale);
        context.setVariable("nome", user.getNome());
        context.setVariable("usuario", user.getUsername());
        context.setVariable("email", user.getEmail());
        context.setVariable("senha", user.getPassword());
		final Email email = new Email();
        email.setSubject(Constantes.getBundle("cadastro.novo.usuario.subject.confirmacao"));
        email.setFromEmail(Constantes.getBundle("email.padrao.remetente.interno"));
        email.setRecipientEmail(user.getEmail());
        email.setPageHTML(Constantes.getBundle("pagina.email.confirmacao.cadastro.novo.usuario"));
        email.setFileName(Constantes.getBundle("nome.imagem.logo.email.site"));
        email.setContentType(Constantes.getBundle("content.type.imagem.logo"));
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream(email.getFileName());
        email.setByteArray(FileUtil.getByteArray(inputStream));
        // referencia utilizada apenas para o envio de arquivo in-line, para arquivo em anexo nao ha necessidade
        context.setVariable("imageResourceName", email.getFileName());
		emailService.sendMailWithInline(context, email);
	}
	
	public List<Usuario> listAll() throws Exception {
		return dao.findAll();
	}

	public List<Usuario> findUsersByRole(Role role) throws Exception {
		return dao.findUsersByRole(role);
	}

	@Transactional
	public void create(Usuario user, List<Role> listRoles) throws Exception {
		dao.save(user);
		user.setRoles(listRoles);
		dao.update(user);
	}

	@Transactional
	public void update(Usuario user) throws Exception {
		dao.update(user);
	}

	@Transactional
	public void remove(Usuario user) throws Exception {
		dao.update(user);
		dao.remove(user);
	}

	public List<Role> listRoles() throws Exception {
		return roleDao.findAll();
	}


	public Role getRoleByDescription(String description) throws Exception {
		return roleDao.getRoleByDescription(description);
	}
	
	public List<Role> listRolesByDescription(String description) throws Exception {
		List<Role> roles = new ArrayList<Role>();
		Role role = roleDao.getRoleByDescription(description);
		if (role != null) {
			roles.add(role);
		}
		return roles;
	}
}
