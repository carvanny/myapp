package br.com.maximuslab.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.maximuslab.dao.MoradorDAO;
import br.com.maximuslab.entity.Morador;

@Service
public class MoradorService {

	@Autowired
	private MoradorDAO dao;

	@Transactional
	public void create(Morador enquete) throws Exception {
		dao.save(enquete);
		//enquete.setRespostasEnquete(enquete.getRespostaEnqueteForm().getRespostasEnquete(enquete));
		//dao.update(enquete);
	}
	
	@Transactional
	public void update(Morador enquete) throws Exception {
		dao.update(enquete);
	}
	
	public Morador getById(Long id) throws Exception {
		return dao.getById(id);
	}
	
	public List<Morador> listAll() throws Exception {
		return dao.findAll();
	}
	
}
