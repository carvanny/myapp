package br.com.maximuslab.entity;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.annotations.GenericGenerator;

/**
*
* @author carvanny
*/
@Entity
@Table(name = "CARGO", catalog = "", schema = "APP")
@XmlRootElement
public class Cargo extends AbstractEntity implements Serializable {

	private static final long serialVersionUID = 1L;
	
    @Id
    @GenericGenerator(name="kaugen" , strategy="increment")
    @GeneratedValue(generator="kaugen")
    @Column(name="ID")
	private long id;
    
    @Column(name = "NOME")
	private String nome;
    
    @Column(name = "DESCRICAO")
	private String descricao;

}
