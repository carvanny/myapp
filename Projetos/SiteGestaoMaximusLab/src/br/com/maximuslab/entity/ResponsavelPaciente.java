package br.com.maximuslab.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.annotations.GenericGenerator;

/**
*
* @author carvanny
*/
@Entity
@Table(name = "RESPONSAVEL_PACIENTE", catalog = "", schema = "APP")
@XmlRootElement
public class ResponsavelPaciente implements Serializable{

	private static final long serialVersionUID = 1L;
	
    @Id
    @GenericGenerator(name="kaugen" , strategy="increment")
    @GeneratedValue(generator="kaugen")
    @Column(name="ID")
	private long id;
	private String nome;
	private String cpf;
	private String rg;
	private Endereco endereco;

}
