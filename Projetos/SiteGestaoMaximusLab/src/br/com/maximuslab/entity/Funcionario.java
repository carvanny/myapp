package br.com.maximuslab.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.annotations.GenericGenerator;

/**
*
* @author carvanny
*/
@Entity
@Table(name = "FUNCIONARIO", catalog = "", schema = "APP")
@XmlRootElement
public class Funcionario extends AbstractEntity implements Serializable {

	private static final long serialVersionUID = 1L;

    @Id
    @GenericGenerator(name="kaugen" , strategy="increment")
    @GeneratedValue(generator="kaugen")
    @Column(name="ID")
	private long id;
    
    @Column(name = "NOME")
	private String nome;
    
    @Column(name = "CARGO")
	private Cargo cargo;
    
    @Column(name = "USUARIO")
    private Usuario usuario;
	
}
