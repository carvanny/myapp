package br.com.maximuslab.entity;

import java.io.Serializable;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "morador")
public class Morador extends AbstractEntity implements Serializable {
	private static final long serialVersionUID = -7590317347612436291L;

	@Column(name = "descricao", nullable = false)
	private String descricao;
	
	@Column(name = "pergunta", nullable = false)
	private String pergunta;
	
	@Column(name = "data_inicio", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern=PATTERN_DATE)
	private Calendar dataInicio;

	@Column(name = "data_fim", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern=PATTERN_DATE)
	private Calendar dataFim;

	@Column(name = "data_criacao", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern=PATTERN_DATE_TIME)
	private Calendar dataCriacao;

	@Column(name = "data_alteracao")
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern=PATTERN_DATE_TIME)
	private Calendar dataAlteracao;
	
	@Column(name = "data_inativacao")
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern=PATTERN_DATE_TIME)
	private Calendar dataInativacao;
	
	@Column(name = "user_criacao", length = 20, nullable = false)
	private String usuarioCriacao;
	
	@Column(name = "user_alteracao", length = 20)
	private String usuarioAlteracao;

	@Column(name = "user_inativacao", length = 20)
	private String usuarioInativacao;
	
	@Transient
	private boolean active;

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getPergunta() {
		return pergunta;
	}

	public void setPergunta(String pergunta) {
		this.pergunta = pergunta;
	}

	public Calendar getDataInicio() {
		return dataInicio;
	}

	public void setDataInicio(Calendar dataInicio) {
		this.dataInicio = dataInicio;
	}

	public Calendar getDataFim() {
		return dataFim;
	}

	public void setDataFim(Calendar dataFim) {
		this.dataFim = dataFim;
	}

	public Calendar getDataCriacao() {
		return dataCriacao;
	}

	public void setDataCriacao(Calendar dataCriacao) {
		this.dataCriacao = dataCriacao;
	}

	public Calendar getDataAlteracao() {
		return dataAlteracao;
	}

	public void setDataAlteracao(Calendar dataAlteracao) {
		this.dataAlteracao = dataAlteracao;
	}

	public String getUsuarioCriacao() {
		return usuarioCriacao;
	}

	public void setUsuarioCriacao(String usuarioCriacao) {
		this.usuarioCriacao = usuarioCriacao;
	}

	public String getUsuarioAlteracao() {
		return usuarioAlteracao;
	}

	public void setUsuarioAlteracao(String usuarioAlteracao) {
		this.usuarioAlteracao = usuarioAlteracao;
	}

	public Calendar getDataInativacao() {
		return dataInativacao;
	}

	public void setDataInativacao(Calendar dataInativacao) {
		this.dataInativacao = dataInativacao;
	}

	public String getUsuarioInativacao() {
		return usuarioInativacao;
	}

	public void setUsuarioInativacao(String usuarioInativacao) {
		this.usuarioInativacao = usuarioInativacao;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}
	
}
