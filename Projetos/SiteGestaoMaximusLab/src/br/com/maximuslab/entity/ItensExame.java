package br.com.maximuslab.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.annotations.GenericGenerator;

/**
*
* @author carvanny
*/
@Entity
@Table(name = "ITENS_EXAME", catalog = "", schema = "APP")
@XmlRootElement
public class ItensExame extends AbstractEntity implements Serializable {

	private static final long serialVersionUID = 1L;

    @Id
    @GenericGenerator(name="kaugen" , strategy="increment")
    @GeneratedValue(generator="kaugen")
    @Column(name="ID")
	private long id;
    
    @Column(name = "NOME")
	private String nome;
    
    @Column(name = "DESCRICAO")
	private String descricao;
    
    @Column(name = "CODIGO")
	private String codigo;
    
    @Column(name = "PRECO")
	private double preco;
	
}
