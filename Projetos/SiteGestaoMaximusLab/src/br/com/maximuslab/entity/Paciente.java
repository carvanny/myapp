package br.com.maximuslab.entity;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;

public class Paciente implements Serializable{

	private static final long serialVersionUID = 1L;

    @Id
    @GenericGenerator(name="kaugen" , strategy="increment")
    @GeneratedValue(generator="kaugen")
    @Column(name="ID")
	private long id;
    
    @Column(name = "CPF")
	private String cpf;
    
    @Column(name = "RG")
	private String rg;
    
    @Column(name = "NOME")
	private String nome;
    
    @Column(name = "TELEFONE")
	private String telefone;
    
    @Column(name = "CELULAR")
	private String celular;
    
    @Column(name = "DATA_NASC")
    @Temporal(TemporalType.DATE)
	private LocalDate dataNasc;
    
    @Column(name = "CELULAR")
	private Endereco endereco;
	private ResponsavelPaciente responsavelPaciente;
}
