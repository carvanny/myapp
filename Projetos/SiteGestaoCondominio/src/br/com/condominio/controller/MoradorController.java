package br.com.condominio.controller;

import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;

import br.com.condominio.entity.Mensagem;
import br.com.condominio.entity.Morador;
import br.com.condominio.entity.TipoMensagem;
import br.com.condominio.entity.User;
import br.com.condominio.service.MoradorService;
import br.com.condominio.util.CalendarUtil;
import br.com.condominio.util.Constantes;

/**
 * Application Morador Controller.
 */
@Controller
@Secured ({"ROLE_MANAGER", "ROLE_ADMIN"})
public class MoradorController {
	
	private static Logger logger = Logger.getLogger(MoradorController.class);
	
	@Autowired
	private MoradorService moradorService;
	
    @RequestMapping("/listaMoradores")
    public String listaMoradores(HttpSession session, User user, Model model, final Mensagem mensagem) {
    	try {
    		session.setAttribute(Constantes.ENQUETES_SESSION, moradorService.listAll());
		} catch (Exception e) {
    		// atualiza as informacoes que serao exibidas na mensagem de erro para o usuario
			mensagem.setTitulo(Constantes.getBundle("enquete.listagem.mensagem.titulo.erro"));
        	mensagem.setTexto(Constantes.getBundle("enquete.listagem.mensagem.texto.erro"));
        	mensagem.setTipoMensagem(TipoMensagem.ERRO);
        	logger.error(e, e.getCause());
		}
        return Constantes.ENQUETE_LISTAR;
    }
	
    @RequestMapping("/cadastraMorador")
    public String cadastraMorador(User user, final Morador enquete, Mensagem mensagem) {
    	enquete.setDataInicio(new GregorianCalendar());
    	enquete.setDataFim(new GregorianCalendar());
    	enquete.setActive(true);
        return Constantes.ENQUETE_MANTER;
    }

    @RequestMapping("/editaMorador")
    public String editaMorador(HttpSession session, User user, final Morador enquete, Mensagem mensagem) {
    	@SuppressWarnings("unchecked")
		List<Morador> enqueteList = (List<Morador>) session.getAttribute(Constantes.ENQUETES_SESSION);
    	if (enqueteList != null) {
    		for (Morador enqueteSession : enqueteList) {
    			if (enqueteSession.getId().equals(enquete.getId())) {
    				enquete.setPergunta(enqueteSession.getPergunta());
    				enquete.setDescricao(enqueteSession.getDescricao());
    				enquete.setDataInicio(enqueteSession.getDataInicio());
    				enquete.setDataFim(enqueteSession.getDataFim());
    				enquete.setDataInativacao(enqueteSession.getDataInativacao());
    				if (enqueteSession.getDataInativacao() != null) {
    					enquete.setActive(false);
    				} else {
    					enquete.setActive(true);
    				}
    				break;
    			}
    		}
    	}
        return Constantes.ENQUETE_MANTER;
    }
    
    @RequestMapping("/gravaMorador")
    public String gravaMorador(HttpSession session, User user, final Morador enquete, final Mensagem mensagem, Model model, Locale locale) {
    	if (validation(enquete)) {
	    	try {
    			enquete.setDataInicio(CalendarUtil.setPrimeiroSegundoDia(enquete.getDataInicio()));
    			enquete.setDataFim(CalendarUtil.setUltimoSegundoDia(enquete.getDataFim()));
	    		if (enquete.getId() == null && CalendarUtil.isDateBeforeActualDate(enquete.getDataInicio())) {
	        		// atualiza as informacoes que serao exibidas na mensagem de erro para o usuario
	            	mensagem.setTextoErro(Constantes.getBundle("enquete.mensagem.erro.validacao.data.inicial.menor.data.atual"));
	    			logger.error("| erro data inicial enquete - menor que a data atual |");
	    			return Constantes.ENQUETE_MANTER;
	    		} 
	    		if (enquete.getDataInicio().after(enquete.getDataFim())) {
	        		// atualiza as informacoes que serao exibidas na mensagem de erro para o usuario
	            	mensagem.setTextoErro(Constantes.getBundle("enquete.mensagem.erro.validacao.data.inicial.menor.data.final"));
	    			logger.error("| erro data inicial enquete - campos obrigatorios nao informados |");
	    			return Constantes.ENQUETE_MANTER;
	    		} else {
		    		user = (User) session.getAttribute(Constantes.USER_SESSION);
		    		if (enquete.getId() == null) {
		    			enquete.setUsuarioCriacao(user.getUsername());
			    		enquete.setDataCriacao(new GregorianCalendar());
			    		if (!enquete.isActive()) enquete.setDataInativacao(new GregorianCalendar());
			    		moradorService.create(enquete);
		    		} else {
		    			Morador enqueteBD = moradorService.getById(enquete.getId());
		    			enqueteBD.setDescricao(enquete.getDescricao());
		    			enqueteBD.setPergunta(enquete.getPergunta());
		    			enqueteBD.setDataInicio(enquete.getDataInicio());
		    			enqueteBD.setDataFim(enquete.getDataFim());
		    			enqueteBD.setUsuarioAlteracao(user.getUsername());
		    			enqueteBD.setDataAlteracao(new GregorianCalendar());
		    			if (enquete.isActive() && enqueteBD.getDataInativacao() != null) {
		    				enqueteBD.setDataInativacao(null);
		    				enqueteBD.setUsuarioInativacao(null);
		    			} else if (!enquete.isActive() && enqueteBD.getDataInativacao() == null) {
		    				enqueteBD.setDataInativacao(new GregorianCalendar());
		    				enqueteBD.setUsuarioInativacao(user.getUsername());
		    			}
			    		moradorService.update(enqueteBD);
		    		}
		    		// atualiza as informacoes que serao exibidas na mensagem de sucesso para o usuario
					mensagem.setTitulo(Constantes.getBundle("enquete.gravacao.mensagem.titulo.sucesso"));
		        	mensagem.setTexto(Constantes.getBundle("enquete.gravacao.mensagem.texto.sucesso"));
		        	logger.info("| enquete gravada com sucesso -> " + enquete.getPergunta() + " |");
		    		mensagem.setTipoMensagem(TipoMensagem.SUCESSO);
	    		}
			} catch (Exception e) {
	    		// atualiza as informacoes que serao exibidas na mensagem de erro para o usuario
				mensagem.setTitulo(Constantes.getBundle("enquete.gravacao.mensagem.titulo.erro"));
	        	mensagem.setTexto(Constantes.getBundle("enquete.gravacao.mensagem.texto.erro"));
	        	mensagem.setTipoMensagem(TipoMensagem.ERRO);
	        	logger.error(e, e.getCause());
	        	return Constantes.ENQUETE_MANTER;
			}
		} else {
    		// atualiza as informacoes que serao exibidas na mensagem de erro para o usuario
        	mensagem.setTextoErro(Constantes.getBundle("mensagem.erro.validacao.campos.obrigatorios.nao.informados"));
			logger.error("| erro na gravacao da enquete - campos obrigatorios nao informados |");
			return Constantes.ENQUETE_MANTER;
		}  		    	
    	return listaMoradores(session, user, model, mensagem);
    }
    
	private boolean validation(Morador enquete) {
		Boolean validation = false;
		if (enquete != null && (!StringUtils.isEmpty(enquete.getDescricao()) && !StringUtils.isEmpty(enquete.getPergunta())) &&
			enquete.getDataInicio() != null && enquete.getDataFim() != null) {
			validation = true;
		}
		return validation;
	}
	
}
