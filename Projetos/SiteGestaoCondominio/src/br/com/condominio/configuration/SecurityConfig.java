package br.com.condominio.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {
	    
	@Override
    protected AuthenticationManager authenticationManager() throws Exception {
		return new CustomAuthenticationManager();
    }
	
    @Override
    public void configure(WebSecurity web) throws Exception {
    	web.ignoring().antMatchers("/resources/**");
    }
    
	@Override
	protected void configure(HttpSecurity http) throws Exception {
	  http
	  	.authorizeRequests()
	  	// conteudo statico das paginas htmls
	  	.antMatchers("/css/**","/font-awesome-4.1.0/**","/fonts/**","/img/**","/js/**","/less/**").permitAll()
	  	// templates htmls para envio de email dinamico
	  	.antMatchers("/email-confirmacao-cadastro-novo-usuario.html",
	  				 "/email-confirmacao-geracao-nova-senha.html",
	  				 "/email-confirmacao-recebimento-faleconosco.html", 
	  				 "/email-confirmacao-troca-senha.html",
	  				 "/email-interno-cancelamento-faleconosco.html",
 				 	 "/email-interno-recebimento-faleconosco.html").permitAll()
	  	// paginas htmls do site
	  	.antMatchers("/", "/index.html", "/layout.html", "/morador_listar.html", "/morador_manter.html").permitAll()
	  	// acoes executadas pelo usuario nao autenticado
	  	.antMatchers("/enviaFaleConosco", "/efetuaLogin", "/regeraSenha", "/cadastraUsuario", 
	  				 "/index", "/login", "/acessoNegado", "/cancelaFaleConosco", "/listaMoradores", "/cadastraMorador",
	  				 "/logout", "/sessaoExpirada", "/404", "/error").permitAll()
	  	// diretorios referentes aos usuarios autenticados na aplicacao e suas permissoes de acesso
		.antMatchers("/user/**").access("hasRole('ROLE_USER') or hasRole('ROLE_MANAGER') or hasRole('ROLE_ADMIN')")
		.antMatchers("/manager/**").access("hasRole('ROLE_MANAGER') or hasRole('ROLE_ADMIN')")
		.antMatchers("/admin/**").access("hasRole('ROLE_ADMIN')")
	  	.anyRequest().authenticated()
	  	.and().formLogin().loginPage("/").permitAll()
	  	.and().formLogin().loginPage("/login").permitAll()
	  	.and().exceptionHandling().accessDeniedPage("/acessoNegado")
	  	.and().sessionManagement()
	  	.sessionAuthenticationErrorUrl("/userIndex")
	  	.invalidSessionUrl("/sessaoExpirada").maximumSessions(1);
	}
	
}