package br.com.condominio.configuration;

import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

@ControllerAdvice
public class ExceptionControllerAdvice {

	private static Logger logger = Logger.getLogger(ExceptionControllerAdvice.class);
	
	@ExceptionHandler(Exception.class)
	public String exception(Exception e) {
		ModelAndView model = new ModelAndView();
		model.addObject("name", e.getClass().getSimpleName());
		model.addObject("message", e.getMessage());
		logger.error(e, e.getCause());
		return "redirect:/error";
	}
}
