package br.com.condominio.util;

import java.util.Calendar;
import java.util.Locale;

public class CalendarUtil {

	public static boolean isDateBeforeActualDate(Calendar dateToCompare) throws Exception {
		if (dateToCompare.before((setPrimeiroSegundoDia(Calendar.getInstance())))) {
			return true;
		}
		return false;
	}
	
	public static Calendar setPrimeiroSegundoDia(Calendar data) throws Exception {
		data.set(Calendar.HOUR_OF_DAY, 0);
		data.set(Calendar.MINUTE, 0);
		data.set(Calendar.SECOND, 0);
		data.set(Calendar.MILLISECOND, 0);
		return data;
	}
	
	public static Calendar setUltimoSegundoDia(Calendar data) throws Exception {
		data.set(Calendar.HOUR_OF_DAY, 23);
		data.set(Calendar.MINUTE, 59);
		data.set(Calendar.SECOND, 59);
		return data;
	}
	
	public static String dateFormat(Calendar date, Locale locale) {
		return new CalendarFormatter().print(date, locale);
	}

}
