package br.com.condominio.util;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;

public class FileUtil {

    public static byte [] getByteArray(InputStream inputStream) throws Exception {
		ByteArrayOutputStream buffer = new ByteArrayOutputStream();
    	int nRead;
    	byte[] data = new byte[16384];
    	while ((nRead = inputStream.read(data, 0, data.length)) != -1) {
    		buffer.write(data, 0, nRead);
    	}
    	buffer.flush();
    	return buffer.toByteArray();
    }
}
