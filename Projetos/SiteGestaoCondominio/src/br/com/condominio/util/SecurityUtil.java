package br.com.condominio.util;

import java.security.MessageDigest;
import java.util.UUID;

import org.apache.log4j.Logger;

public class SecurityUtil {
	
	public static final String APP_SECRET_KEY = encryptPasswordKey(".132@&3yx");
	
	private static Logger logger = Logger.getLogger(SecurityUtil.class);

	public static String encryptPassword(String password) throws Exception {
		String newPassword = null;
		try {
			MessageDigest messageDigest = MessageDigest.getInstance("SHA-512");
			byte byteHash[] = messageDigest.digest(password.getBytes("UTF-8"));
			StringBuilder hexHash = new StringBuilder();
			for(byte b : byteHash) {
				hexHash.append(String.format("%02X", 0xFF & b));
			}
			newPassword = hexHash.toString();
		} catch (Exception e) {
			logger.error(e, e.getCause());
			throw new Exception(e);
		}
		return newPassword;
	}

	public static String passwordGenerate() throws Exception {
		return UUID.randomUUID().toString().substring(0,6);
	}
	
	private static String encryptPasswordKey(String password) {
		String newPassword = password;
		try {
			newPassword = encryptPassword(password);
		} catch (Exception e) {
			logger.error(e, e.getCause());
		}
		return newPassword;
	}
}
