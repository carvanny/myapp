package br.com.condominio.util;

import java.util.Locale;
import java.util.ResourceBundle;


public class Constantes {

	// ============================================================================================================================================================
	// Mapeamento dos diretorios e das views (paginas HTMLs) da aplicacao
	// ============================================================================================================================================================
	// Diretorios das views
	public static String DIRETORIO_VIEWS			= "WEB-INF/views/";
	public static String DIRETORIO_USER				= DIRETORIO_VIEWS + "user/";
	public static String DIRETORIO_MANAGER			= DIRETORIO_VIEWS + "manager/";
	public static String DIRETORIO_ADMIN			= DIRETORIO_VIEWS + "admin/";
	// pagina inicial (visivel para todos os usuarios)
	public static String INDEX						= DIRETORIO_VIEWS + "index";
	// paginas visiveis para o usuario basico (ROLE_USER) autenticado na aplicacao
	public static String USER_INDEX					= DIRETORIO_USER + "index";
	// paginas visiveis para o usuario gerente (ROLE_MANAGER) autenticado na aplicacao
	public static String MANAGER_INDEX				= DIRETORIO_MANAGER + "index";
	// paginas visiveis para o usuario admin (ROLE_ADMIN) autenticado na aplicacao
	public static String ADMIN_INDEX				= DIRETORIO_ADMIN + "index";
	public static String ENQUETE_MANTER				= DIRETORIO_MANAGER + "morador_manter";
	public static String ENQUETE_LISTAR				= DIRETORIO_MANAGER + "morador_listar";
	// redirect actions
	public static String REDIRECT_SESSAO_EXPIRADA 	= "redirect:/sessaoExpirada";
	public static String REDIRECT_USER_INDEX	 	= "/userIndex";
	// ============================================================================================================================================================
	// Perfil do usuario da aplicacao
	// ============================================================================================================================================================
	public static String ROLE_USER 		= "ROLE_USER";
	public static String ROLE_MANAGER 	= "ROLE_MANAGER";
	public static String ROLE_ADMIN 	= "ROLE_ADMIN";

	// ============================================================================================================================================================
	// Mensagens do Banco de Dados
	// ============================================================================================================================================================
	// codigo do erro da exception retornada do banco de dados
	public static int ERROR_CODE_CONSTRAINT_VIOLATION_EXCEPTION = 1062;
	
	// valor verdadeiro para o campo no banco de dados
	public static String STRING_VALOR_CAMPO_VERDADEIRO = "S";

	// valor falso para o campo no banco de dados
	public static String STRING_VALOR_CAMPO_FALSO = "";

	// ============================================================================================================================================================
	// Parametros da sessao da aplicacao
	// ============================================================================================================================================================
	// usuario da sessao
	public static String USER_SESSION = "userSession";
	// enquetes da sessao
	public static String ENQUETES_SESSION = "enquetesSession";
	
	// exibe a modal login do usuario
	public static String PARAMETRO_EXIBE_LOGIN_MODAL 				= "exibeLoginModal";
	// exibe a modal geracao nova senha do usuario
	public static String PARAMETRO_EXIBE_GERACAO_NOVA_SENHA_MODAL 	= "exibeEsqueciSenhaModal";
	// exibe a modal cadastro de usuario
	public static String PARAMETRO_EXIBE_CADASTRO_USUARIO_MODAL 	= "exibeCadastroModal";
	// exibe a modal troca senha do usuario
	public static String PARAMETRO_EXIBE_TROCA_SENHA_MODAL 			= "exibeTrocaSenhaModal";
	// exibe a modal seleciona perfil do usuario
	public static String PARAMETRO_EXIBE_SELECIONA_PERFIL_MODAL 	= "exibeSelecionaPerfilModal";
	// exibe a modal enquete
	public static String PARAMETRO_EXIBE_ENQUETE_MODAL 				= "exibeEnqueteModal";
	// valor do paramentro se exibicao
	public static String VALOR_PARAMETRO_EXIBE_MODAL 				= "show";
	

	// instancia o arquivo de propriedades da aplicacao
	private static ResourceBundle bundle = ResourceBundle.getBundle("messages", new Locale("pt_BR"));

	// metodo responsavel por recuperar a mensagem do arquivo de propriedades
	public static String getBundle(String key) {
		return bundle.getString(key);
	}

}
