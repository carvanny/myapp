package br.com.condominio.util;

import org.apache.log4j.Logger;

public class StringUtil {
	
	private static Logger logger = Logger.getLogger(StringUtil.class);

	public static String criaURL(String acao) {
		StringBuffer url = new StringBuffer();
		try {
			url
			.append(Constantes.getBundle("application.web.site.url"))
			.append(acao);
		} catch (Exception e) {
			logger.error(e, e.getCause());
		}
		return url.toString();
	}
}
