package br.com.condominio.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import org.springframework.format.Formatter;

public class CalendarFormatter implements Formatter<Calendar> {

	private SimpleDateFormat simpleDateFormat;
	
	public Calendar parse(String text, Locale locale) throws ParseException {
		createDateFormat();
		final Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(simpleDateFormat.parse(text).getTime());
		return calendar;
	}

	public String print(Calendar object, Locale locale) {
		createDateFormat();
		return simpleDateFormat.format(object.getTime());
	}

    private void createDateFormat() {
    	if (simpleDateFormat == null) {
	        this.simpleDateFormat = new SimpleDateFormat(Constantes.getBundle("date.format"));
	        this.simpleDateFormat.setLenient(false);
    	}
    }

}
