package br.com.condominio.dao;

import java.lang.reflect.ParameterizedType;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.PlatformTransactionManager;

import br.com.condominio.entity.AbstractEntity;

@SuppressWarnings("unchecked")
public abstract class AbstractGenericDAO<T extends AbstractEntity> {

	private JdbcTemplate jdbcTemplate;
	private PlatformTransactionManager platformTransactionManager;

	@PersistenceContext
	private EntityManager entityManager;

	public T getById(final Long id) {
		return (T) entityManager.find(getTypeClass(), id);
	}

	public List<T> findAll() {
		return entityManager.createQuery("FROM " + getTypeClass().getName())
				.getResultList();
	}

	public void save(T entity) {
		entityManager.persist(entity);
		entityManager.flush();
	}

	public void update(T entity) {
		entityManager.merge(entity);
		entityManager.flush();
	}

	public void remove(T entity) {
		T entityToRemove = getById(entity.getId());
		entityManager.remove(entityToRemove);
		entityManager.flush();
	}

	private Class<?> getTypeClass() {
		Class<?> clazz = (Class<?>) ((ParameterizedType) this.getClass()
				.getGenericSuperclass()).getActualTypeArguments()[0];
		return clazz;
	}

	public EntityManager getEntityManager() {
		return entityManager;
	}

	@SuppressWarnings("rawtypes")
	public CriteriaQuery getCriteriaQuery() {
		return entityManager.getCriteriaBuilder().createQuery(getTypeClass());
	}

	@SuppressWarnings("rawtypes")
	public Root getRoot(CriteriaQuery criteriaQuery) {
		return criteriaQuery.from(getTypeClass());
	}

	public void setPlatformTransactionManager(PlatformTransactionManager platformTransactionManager) {
	  this.platformTransactionManager = platformTransactionManager;
	}
	
	public PlatformTransactionManager getPlatformTransactionManager() {
		return platformTransactionManager;
	}

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
	  this.jdbcTemplate = jdbcTemplate;
	}

	public JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}

}
