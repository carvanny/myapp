package br.com.condominio.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.condominio.entity.Role;
import br.com.condominio.entity.User;

@Repository
@Transactional
public class UserDAO extends AbstractGenericDAO<User> implements Serializable {
	private static final long serialVersionUID = 6101614003648606841L;

	@SuppressWarnings({"rawtypes", "unchecked"})
	public User login(String emailUsername, String password) {
		CriteriaQuery criteriaQuery = getCriteriaQuery();
		Root root = getRoot(criteriaQuery);
		criteriaQuery.select(root);

		Predicate predicateUserName = getEntityManager().getCriteriaBuilder().equal(root.get("username"), emailUsername);
		Predicate predicateEmail 	= getEntityManager().getCriteriaBuilder().equal(root.get("email"), emailUsername);
		Predicate predicatePassword = getEntityManager().getCriteriaBuilder().equal(root.get("password"), password);
		
		Predicate predicateUserPassword  = getEntityManager().getCriteriaBuilder().and(predicateUserName, predicatePassword);
		Predicate predicateEmailPassword = getEntityManager().getCriteriaBuilder().and(predicateEmail, predicatePassword);
		criteriaQuery.where(getEntityManager().getCriteriaBuilder().or(predicateUserPassword, predicateEmailPassword));
		
		List list = getEntityManager().createQuery(criteriaQuery).getResultList();
		
		User user = null;
		if (list != null && list.size() > 0) {
			user = (User) list.get(0);
		}
		return user;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public User searchEmail(String email) {
		CriteriaQuery criteriaQuery = getCriteriaQuery();
		Root root = getRoot(criteriaQuery);
		criteriaQuery.select(root);

		criteriaQuery.where(getEntityManager().getCriteriaBuilder().equal(root.get("email"), email));
		
		List list = getEntityManager().createQuery(criteriaQuery).getResultList();

		if (list == null || list.isEmpty() || list.size() > 1) {
			return null;
		}
		return (User) list.get(0);
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public User existeEmail(String email) {
		CriteriaQuery criteriaQuery = getCriteriaQuery();
		Root root = getRoot(criteriaQuery);
		criteriaQuery.select(root);

		criteriaQuery.where(getEntityManager().getCriteriaBuilder().equal(root.get("email"), email));
		
		List list = getEntityManager().createQuery(criteriaQuery).getResultList();

		User user = null;
		if (list != null && list.size() > 0) {
			user = (User) list.get(0);
		}
		return user;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public User existeUsername(String username) {
		CriteriaQuery criteriaQuery = getCriteriaQuery();
		Root root = getRoot(criteriaQuery);
		criteriaQuery.select(root);

		criteriaQuery.where(getEntityManager().getCriteriaBuilder().equal(root.get("username"), username));
		
		List list = getEntityManager().createQuery(criteriaQuery).getResultList();

		User user = null;
		if (list != null && list.size() > 0) {
			user = (User) list.get(0);
		}
		return user;
	}

	public List<User> findUsersByRole(Role role) {
		List<User> usersRole = new ArrayList<User>();
		List<User> listAll = findAll();
		for (User user: listAll) {
			if (!user.getRoles().isEmpty() && user.getRoles().size() == 1) {
				Role userRole = user.getRoles().get(0);
				if (userRole.getId().equals(role.getId())) {
					usersRole.add(user);
				}
			}
		}
		return usersRole;
	}

}
