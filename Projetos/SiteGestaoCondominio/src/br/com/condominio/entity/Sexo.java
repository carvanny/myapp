package br.com.condominio.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "sexo")
public class Sexo extends AbstractEntity implements Serializable {
	private static final long serialVersionUID = -7590317347612436291L;

	@Column(name = "descricao", nullable = false)
	private String descricao;
	
	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

}
