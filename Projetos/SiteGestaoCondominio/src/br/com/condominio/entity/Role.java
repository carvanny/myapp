package br.com.condominio.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.security.core.GrantedAuthority;

@Entity
@Table(name="role")
public class Role extends AbstractEntity implements GrantedAuthority {
	private static final long serialVersionUID = -3968396919486158590L;

	@Column(name="description", unique=true)
	private String description;

	@Column(name="name", nullable = false)
	private String name;

	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Transient
	public String getAuthority() {
		return description;
	}
}
