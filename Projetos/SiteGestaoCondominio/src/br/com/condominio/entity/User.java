package br.com.condominio.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

@Entity
@Table(name = "user")
public class User extends AbstractEntity implements Serializable, UserDetails {
	private static final long serialVersionUID = -7590317347612436291L;
	
	@Column(name = "nome", nullable = false)
	private String nome;

	@Column(name = "sobre_nome", nullable = false)
	private String sobrenome;

	@ManyToOne
	@JoinColumn(name = "id_sexo", nullable = false)
	private Sexo sexo;

	@Column(name = "data_nascimento", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern=PATTERN_DATE)
	private Calendar dataNascimento;

	@Column(name = "email", unique = true, nullable = false)
	private String email;

	@Column(name = "telefone", length = 20)
	private String telefone;

	@Column(name = "celular", length = 20)
	private String celular;

	@Column(name = "user_name", unique = true, nullable = false, length = 20)
	private String username;

	@Column(name = "data_inclusao", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern=PATTERN_DATE_TIME)
	private Calendar dataInclusao;

	@Column(name = "data_alteracao")
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern=PATTERN_DATE_TIME)
	private Calendar dataAlteracao;

	@Column(name = "data_inativacao")
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern=PATTERN_DATE_TIME)
	private Calendar dataInativacao;

	@Column(name = "motivo_inativacao")
	private String motivoInativacao;

	@Column(name = "user_password", nullable = false)
	private String password;

	@Column(name = "nova_senha", length = 1)
	private String novaSenha;

	@Column(name = "user_alteracao", length = 20)
	private String usuarioAlteracao;

	@Column(name = "user_inativacao", length = 20)
	private String usuarioInativacao;
	
	@Transient
	private String confirmacaoNovaSenha;

	@Transient
	private Role currentRole;
	
	@Transient
	private List<GrantedAuthority> listGrantedAuthorities;
	
	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getSobrenome() {
		return sobrenome;
	}

	public void setSobrenome(String sobrenome) {
		this.sobrenome = sobrenome;
	}

	public Sexo getSexo() {
		return sexo;
	}

	public void setSexo(Sexo sexo) {
		this.sexo = sexo;
	}

	public Calendar getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(Calendar dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public Calendar getDataInclusao() {
		return dataInclusao;
	}

	public void setDataInclusao(Calendar dataInclusao) {
		this.dataInclusao = dataInclusao;
	}

	public Calendar getDataAlteracao() {
		return dataAlteracao;
	}

	public void setDataAlteracao(Calendar dataAlteracao) {
		this.dataAlteracao = dataAlteracao;
	}

	public Calendar getDataInativacao() {
		return dataInativacao;
	}

	public void setDataInativacao(Calendar dataInativacao) {
		this.dataInativacao = dataInativacao;
	}

	public String getMotivoInativacao() {
		return motivoInativacao;
	}

	public void setMotivoInativacao(String motivoInativacao) {
		this.motivoInativacao = motivoInativacao;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getNovaSenha() {
		return novaSenha;
	}

	public void setNovaSenha(String novaSenha) {
		this.novaSenha = novaSenha;
	}

	public Role getCurrentRole() {
		return currentRole;
	}

	public void setCurrentRole(Role currentRole) {
		this.currentRole = currentRole;
	}

	@ManyToMany(cascade = {CascadeType.ALL})
	@JoinTable(name = "user_role", joinColumns = {@JoinColumn(name = "user_id")}, inverseJoinColumns = {@JoinColumn(name = "role_id")})
	@LazyCollection(LazyCollectionOption.FALSE)
	private List<Role> roles;

	public List<Role> getRoles() {
		return roles;
	}

	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public String getCelular() {
		return celular;
	}

	public void setCelular(String celular) {
		this.celular = celular;
	}

	public String getUsuarioAlteracao() {
		return usuarioAlteracao;
	}

	public void setUsuarioAlteracao(String usuarioAlteracao) {
		this.usuarioAlteracao = usuarioAlteracao;
	}

	public String getUsuarioInativacao() {
		return usuarioInativacao;
	}

	public void setUsuarioInativacao(String usuarioInativacao) {
		this.usuarioInativacao = usuarioInativacao;
	}

	@Transient
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return roles;
	}

	@Transient
	public boolean isAccountNonExpired() {
		return false;
	}

	@Transient
	public boolean isAccountNonLocked() {
		return false;
	}

	@Transient
	public boolean isCredentialsNonExpired() {
		return false;
	}

	@Transient
	public boolean isEnabled() {
		if (getDataInativacao() == null) {
			return true;
		}
		return false;
	}

	public String getConfirmacaoNovaSenha() {
		return confirmacaoNovaSenha;
	}

	public void setConfirmacaoNovaSenha(String confirmacaoNovaSenha) {
		this.confirmacaoNovaSenha = confirmacaoNovaSenha;
	}
	
	public List<GrantedAuthority> getListGrantedAuthorities() {
		if (listGrantedAuthorities == null) {
			List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
			for (Role role : getRoles()) {
				authorities.add(new SimpleGrantedAuthority(role.getDescription()));
			}
			setListGrantedAuthorities(authorities);
		}
		return listGrantedAuthorities;
	}

	public void setListGrantedAuthorities(List<GrantedAuthority> listGrantedAuthorities) {
		this.listGrantedAuthorities = listGrantedAuthorities;
	}

	public void reset() {
		setNome(null);
		setEmail(null);
		setTelefone(null);
	}
}
