package br.com.condominio.entity;

public class Mensagem {

	private String titulo 		= null;
	private String texto 		= null;
	private String textoModal 	= null;
	private String textoErro 	= null;
	private String textoErro1 	= null;
	private String textoErro2 	= null;
	private TipoMensagem tipoMensagem = TipoMensagem.SUCESSO;

	public Mensagem() {
		super();
	}

	/**
	 * Define os valores para todos os campos da mensagem
	 * @param titulo
	 * @param mensagem
	 * @param tipoMensagem
	 */
	public Mensagem(String titulo, String texto, TipoMensagem tipoMensagem) {
		super();
		this.titulo = titulo;
		this.texto = texto;
		this.tipoMensagem = tipoMensagem;
	}

	/**
	 * Define apenas os valores para os campos titulo e texto, o campo tipoMensagem utilizara o valor "SUCESSO"
	 * @param titulo
	 * @param mensagem
	 */
	public Mensagem(String titulo, String texto) {
		super();
		this.titulo = titulo;
		this.texto = texto;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getTexto() {
		return texto;
	}

	public void setTexto(String texto) {
		this.texto = texto;
	}

	public TipoMensagem getTipoMensagem() {
		return tipoMensagem;
	}

	public void setTipoMensagem(TipoMensagem tipoMensagem) {
		this.tipoMensagem = tipoMensagem;
	}

	public String getTextoModal() {
		return textoModal;
	}

	public void setTextoModal(String textoModal) {
		this.textoModal = textoModal;
	}

	public String getTextoErro() {
		return textoErro;
	}

	public void setTextoErro(String textoErro) {
		this.textoErro = textoErro;
	}

	public String getTextoErro1() {
		return textoErro1;
	}

	public void setTextoErro1(String textoErro1) {
		this.textoErro1 = textoErro1;
	}

	public String getTextoErro2() {
		return textoErro2;
	}

	public void setTextoErro2(String textoErro2) {
		this.textoErro2 = textoErro2;
	}

}