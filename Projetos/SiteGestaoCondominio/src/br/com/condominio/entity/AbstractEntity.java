package br.com.condominio.entity;

import java.io.Serializable;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class AbstractEntity implements Serializable, Cloneable {
	private static final long serialVersionUID = 8493376256203827154L;
	public static final String PATTERN_DATE 	 = "dd/MM/yyyy";
	public static final String PATTERN_TIME 	 = "HH:mm:ss";
	public static final String PATTERN_DATE_TIME = "dd/MM/yyyy HH:mm:ss";

	@Id
	@GeneratedValue
	private Long id;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AbstractEntity other = (AbstractEntity) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public AbstractEntity clone() throws CloneNotSupportedException {  
        return (AbstractEntity) super.clone();
	} 
}
