/* criando o schema default
CREATE SCHEMA `gestaocondominio` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
 *
 * criando o usuario que sera associado ao schema
CREATE USER userdbcondominio@localhost;
 * 
 * alterando a senha do novo usuario
SET PASSWORD FOR userdbcondominio@localhost = PASSWORD('senha');
 *
 * dando permissao ao novo usuario sobre o schema gestaocondominio
GRANT ALL ON gestaocondominio.* TO 'userdbcondominio'@'localhost';
 *
 */

/* populando as regras dos perfis dos usuarios */
insert into role (id, description, name) values (1, "ROLE_ADMIN", "Administrador");
insert into role (id, description, name) values (2, "ROLE_MANAGER", "Gerente");
insert into role (id, description, name) values (3, "ROLE_USER", "Usuário");

/* populando os sexos */
insert into sexo (descricao) values ("Feminino");
insert into sexo (descricao) values ("Masculino");

/* populando os usuarios principais da aplicacao */
insert into user (id, nome, sobre_nome, user_name, id_sexo, email, user_password, data_nascimento, data_inclusao) values (1, "Neuber", "Condomínio Barra mais", "neuber", 2, "neuber.paiva@gmail.com", "C7AD44CBAD762A5DA0A452F9E854FDC1E0E7A52A38015F23F3EAB1D80B931DD472634DFAC71CD34EBC35D16AB7FB8A90C81F975113D6C7538DC69DD8DE9077EC", SYSDATE(), SYSDATE());
insert into user (id, nome, sobre_nome, user_name, id_sexo, email, user_password, data_nascimento, data_inclusao) values (2, "Gerente", "Condomínio Barra mais", "celio", 1, "gerente@barramais.com.br",  "5FC2CA6F085919F2F77626F1E280FAB9CC92B4EDC9EDC53AC6EEE3F72C5C508E869EE9D67A96D63986D14C1C2B82C35FF5F31494BEA831015424F59C96FFF664", SYSDATE(), SYSDATE());
insert into user (id, nome, sobre_nome, user_name, id_sexo, email, user_password, data_nascimento, data_inclusao) values (3, "Morador", "Condomínio Barra mais", "morador", 2, "morador@barramais.com.br", "B14361404C078FFD549C03DB443C3FEDE2F3E534D73F78F77301ED97D4A436A9FD9DB05EE8B325C0AD36438B43FEC8510C204FC1C1EDB21D0941C00E9E2C1CE2", SYSDATE(), SYSDATE());

/* populando as regras dos perfis dos usuarios para cada usuario principal */
insert into user_role values (1, 1);
insert into user_role values (1, 2);
insert into user_role values (1, 3);
insert into user_role values (2, 2);
insert into user_role values (2, 3);
insert into user_role values (3, 3);

